import React, { Component } from "react";
import Navbar from "./navbar";
import FrontScreen from "./frontscreen";
import { Route, Switch, Redirect } from "react-router-dom";
import Movie from "./movie";
import PaymentPage from "./paymentPage";

import Seat from "./seat";

class Main extends Component {
  state = {
    allData: {
      city: "",
      movieId: "",
      cId: "",
      timeId: "",
      dateId: "",
      time: "",
      movie: "",
      hall: "",
      total: "",
      tickets: "",
      date: "",
      time: "",
      amount: "",
      votes: ""
    }
  };

  handleData = data => {
    console.log("main", data);
    this.state.allData = data;
  };
  render() {
    return (
      <div>
        <div>
          <Switch>
            <Route
              path="/bookMovie/:city/:id/buyTicket/:cIndex/:timeIndex/:date"
              render={props => <Seat sendData={this.handleData} {...props} />}
            />
            <Route
              path="/payment"
              render={props => (
                <PaymentPage allData={this.state.allData} {...props} />
              )}
            />
            <Route
              path="/home/:city/:id"
              render={props => (
                <React.Fragment>
                  <Navbar {...props} /> <Movie {...props} />
                </React.Fragment>
              )}
            />
            <Route
              path="/home/:city"
              render={props => (
                <React.Fragment>
                  <Navbar {...props} /> <FrontScreen {...props} />
                </React.Fragment>
              )}
            />
            <Route
              path="/home/NCR"
              render={props => (
                <React.Fragment>
                  <Navbar {...props} /> <FrontScreen {...props} />
                </React.Fragment>
              )}
            />
            <Redirect to="/" exact to="/home/NCR" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Main;
