import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./movie.css";

class Movie extends Component {
  state = {
    day: 0,
    priceData: [
      { name: "Rs. 0-100", isSelected: false, value: 0 },
      { name: "Rs. 101-200", isSelected: false, value: 100 },
      { name: "Rs. 201-300", isSelected: false, value: 200 },
      { name: "More Than 300", isSelected: false, value: 300 }
    ],
    showData: [
      { name: "Morning", isSelected: false, value: "12 AM-11:59 AM" },
      { name: "Afternoon", isSelected: false, value: "12 PM-3:59 PM" },
      { name: "Evening", isSelected: false, value: "4:00 PM-6:59 PM" },
      { name: "Night", isSelected: false, value: "7:00 PM-11:59 PM" }
    ]
  };

  handleChange = e => {
    const { currentTarget: input } = e;
    let priceData = [...this.state.priceData];
    let showData = [...this.state.showData];
    if (input.type === "checkbox") {
      if (input.id === "price") {
        let cb = this.state.priceData.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
      } else if (input.id === "showtime") {
        let cb = this.state.showData.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
      }
    }
    console.log(showData);
    this.setState({ priceData, showData });
  };

  componentDidMount() {
    //for today
    let today = new Date();
    let todayDate = today.getDate();
    let month = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "June",
      "July",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    let tM = today.getMonth();
    let ty = today.getFullYear();
    let todayMonth = month[tM];
    let todayData = { date: todayDate, month: "Today", day: 0 };
    //for next day
    let next = new Date(ty, tM, todayDate + 1);
    let nextDate = next.getDate();
    let nM = next.getMonth();
    let ny = next.getFullYear();
    let nextMonth = month[nM];
    let nextData = { date: nextDate, month: nextMonth, day: 1 };
    //for third day
    let third = new Date(ny, nM, nextDate + 1);
    let thirdDate = third.getDate();
    let tMon = third.getMonth();
    let tyear = third.getFullYear();
    let thirdMonth = month[tMon];
    let thirdData = { date: thirdDate, month: thirdMonth, day: 2 };
    this.setState({ todayData, nextData, thirdData });
  }
  async componentWillMount() {
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    let apiEndPint =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/movies/";

    apiEndPint = apiEndPint + city + "/" + id;
    const { data: moviesData } = await axios.get(apiEndPint);
    this.setState({ moviesData: moviesData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    let apiEndPint =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/movies/";

    apiEndPint = apiEndPint + city + "/" + id;
    if (currProps !== prevProps) {
      console.log("check prevProps and currProps", currProps === prevProps);
      const { data: moviesData } = await axios.get(apiEndPint);
      this.setState({ moviesData: moviesData });
    }
  }

  selectDate = d => {
    this.setState({ day: d });
  };

  handleLike = (day, index) => {
    const movies = { ...this.state.moviesData };
    movies.showTiming[day][index].liked = !movies.showTiming[day][index].liked;
    this.setState({ moviesData: movies });
  };

  render() {
    const id = this.props.match.params.id;
    const city = this.props.match.params.city;
    let day = this.state.day;
    let date;
    const { todayData, nextData, thirdData } = this.state;

    if (day === 0) date = new Date().getDate() + " " + "Today";
    if (day === 1) date = nextData.date + " " + nextData.month;
    if (day === 2) date = thirdData.date + " " + thirdData.month;
    if (this.state.moviesData) {
      //console.log("dd", this.state.moviesData);
      // console.log("dd", this.state.moviesData[id].showTiming);
      // console.log("dd", this.state.moviesData[id].showTiming[0]);
      // console.log("dd", this.state.moviesData[id].showTiming[0].timings);
    }
    return (
      <div>
        {this.state.moviesData && (
          <div>
            <div className="bg-secondary ">
              <br />
              <h3 className="text-white ml-3">{this.state.moviesData.title}</h3>
              <div className="row">
                <span className="text-white ml-3 ">
                  <h5 className="ml-3 mt-1">
                    <i className="fa fa-heart text-danger" aria-hidden="true" />
                    <span className="ml-3">{this.state.moviesData.rating}</span>
                  </h5>
                </span>
                <span
                  className="ml-4 mt-2  border text "
                  style={{
                    borderRadius: "10px",
                    fontSize: "15px",
                    color: "#dce0dd"
                  }}
                >
                  <span className="m-3 mt-2"> ACTIONS </span>
                </span>
                <span
                  className="ml-4 mt-2  border text "
                  style={{
                    borderRadius: "10px",
                    fontSize: "15px",
                    color: "#dce0dd"
                  }}
                >
                  <span className="m-3 mt-2"> THRILLER </span>
                </span>
              </div>
              <span className="text-white ml-3" style={{ fontSize: "10px" }}>
                {this.state.moviesData.votes} votes
              </span>
            </div>
            <div className="bg-secondary content-center row">
              <div className="d-xs-block col-12 col-lg-5 bg-light">
                {day === todayData.day ? (
                  <span>
                    <button
                      className=" btn dateSelected m-3 p-1"
                      onClick={() => this.selectDate(todayData.day)}
                    >
                      {todayData.date} {todayData.month}
                    </button>
                  </span>
                ) : (
                  <span>
                    <button
                      className=" btn datebtn m-3 p-1"
                      onClick={() => this.selectDate(todayData.day)}
                    >
                      {todayData.date} {todayData.month}
                    </button>
                  </span>
                )}
                {day === nextData.day ? (
                  <span>
                    <button
                      className=" btn dateSelected m-3 p-1"
                      onClick={() => this.selectDate(nextData.day)}
                    >
                      {nextData.date} {nextData.month}
                    </button>
                  </span>
                ) : (
                  <span>
                    <button
                      className=" btn datebtn m-3 p-1"
                      onClick={() => this.selectDate(nextData.day)}
                    >
                      {nextData.date} {nextData.month}
                    </button>
                  </span>
                )}
                {day === thirdData.day ? (
                  <span>
                    <button
                      className=" btn dateSelected m-3 p-1"
                      onClick={() => this.selectDate(thirdData.day)}
                    >
                      {thirdData.date} {thirdData.month}
                    </button>
                  </span>
                ) : (
                  <span>
                    <button
                      className=" btn datebtn m-3 p-1"
                      onClick={() => this.selectDate(thirdData.day)}
                    >
                      {thirdData.date} {thirdData.month}
                    </button>
                  </span>
                )}
                <br />
              </div>
              <div className="d-none d-lg-block col-lg-2  bg-light mr-1">
                <div className="" style={{ fontSize: "13px" }}>
                  <div className="text-dark dropdown ml-2">
                    <div className="dropbtn row">
                      Filter Price
                      <i
                        className="fa fa-chevron-down mt-1 ml-1 "
                        style={{ fontSize: "10px", color: "lightgrey" }}
                        id="onhover"
                      />
                    </div>
                    <div className="dropdown-content">
                      {this.state.priceData.map((m, index) => (
                        <div
                          className="  form-check  m-1 ml-2"
                          style={{ margin: "10px" }}
                          key={index}
                        >
                          <input
                            value={m.isSelected}
                            onChange={this.handleChange}
                            type="checkbox"
                            id="price"
                            name={m.name}
                            checked={m.isSelected}
                            className="form-check-input"
                          />
                          <label className="form-check-label" htmlFor={m.name}>
                            {m.name}
                          </label>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
              <div className="d-none d-lg-block col-lg-2  bg-light mr-1">
                <div className="" style={{ fontSize: "13px" }}>
                  <div className="text-dark dropdown ml-2">
                    <div className="dropbtn row">
                      Filter Showtime
                      <i
                        className="fa fa-chevron-down mt-1 ml-1 "
                        style={{ fontSize: "10px", color: "lightgrey" }}
                        id="onhover"
                      />
                    </div>
                    <div className="dropdown-content">
                      {this.state.showData.map((m, index) => (
                        <div
                          className="  form-check  m-1 ml-2"
                          style={{ margin: "10px" }}
                          key={index}
                        >
                          <input
                            value={m.isSelected}
                            onChange={this.handleChange}
                            type="checkbox"
                            id="showtime"
                            name={m.name}
                            checked={m.isSelected}
                            className="form-check-input"
                          />
                          <label className="form-check-label" htmlFor={m.name}>
                            {m.name}
                          </label>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col bg-light"></div>
            </div>
            <div className="row bg-white">
              <div className="d-xs-block col-12 col-lg-9">
                <div className="row">
                  <div className="col-6" style={{ backgroundColor: "#FFA07A" }}>
                    <svg
                      className="ml-3"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns={"http://www.w3.org/1999/xlink"}
                      width="40"
                      height="45"
                      viewBox="0 0 130 45"
                      xml={"preserve"}
                      style={{ fill: "green" }}
                    >
                      <path d="M73.5 95.2H26.8c-1.3 0-2.3-1-2.3-2.3V7.6c0-1.3 1-2.3 2.3-2.3h46.7c1.3 0 2.3 1 2.3 2.3V93c0 1.2-1.1 2.2-2.3 2.2zM26.8 6.4c-.6 0-1.1.5-1.1 1.1v85.4c0 .6.5 1.1 1.1 1.1h46.7c.6 0 1.1-.5 1.1-1.1V7.6c0-.6-.5-1.1-1.1-1.1l-46.7-.1z"></path>
                      <path d="M68.8 78.9H31.3c-.6 0-1.2-.5-1.2-1.2V16c0-.6.5-1.2 1.2-1.2h37.5c.6 0 1.2.5 1.2 1.2v61.7c0 .6-.5 1.2-1.2 1.2zm0-62.9H31.3v61.6h37.4l.1-61.6zm-19 74.4c-2.3 0-4.1-1.9-4.1-4.1 0-2.3 1.9-4.1 4.1-4.1 2.3 0 4.1 1.9 4.1 4.1a4 4 0 0 1-4.1 4.1zm0-7.1a2.9 2.9 0 1 0 2.9 2.9c.1-1.5-1.2-2.9-2.9-2.9zm-5.5-72.1h-1.6c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h1.6c.3 0 .6.3.6.6s-.3.6-.6.6zm11 0h-6.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h6.5c.3 0 .6.3.6.6s-.2.6-.6.6z"></path>
                    </svg>
                    <br />
                    <span style={{ fontSize: "10px" }} className="ml-3">
                      M-Ticket Available{" "}
                    </span>
                  </div>
                  <div
                    className="col-5 ml-1"
                    style={{ backgroundColor: "#FFA07A" }}
                  >
                    <svg
                      className="ml-3"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlns={"http://www.w3.org/1999/xlink"}
                      width="40"
                      height="45"
                      viewBox="0 0 102 102"
                      xml={"preserve"}
                      style={{ fill: "green" }}
                    >
                      <path
                        fill="#F90"
                        d="M58.7 85.9H8.5c-4.1 0-7.5-3.4-7.5-7.5v-1.1c0-4.1 3.4-7.5 7.5-7.5h50.2c4.1 0 7.5 3.4 7.5 7.5v1.1c0 4.1-3.4 7.5-7.5 7.5zM8.5 74.3a3 3 0 0 0-3 3v1.1a3 3 0 0 0 3 3h50.2a3 3 0 0 0 3-3v-1.1a3 3 0 0 0-3-3H8.5zM54.8 99H12.4c-3.7 0-6.8-3-6.8-6.8v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c0 1.3 1.1 2.3 2.4 2.3h42.4c1.3 0 2.3-1.1 2.3-2.3v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c.1 3.8-3 6.8-6.7 6.8zm42-57.9c-1.2 0-2.2-1-2.2-2.2v-2.8c0-.6-.5-1-1-1h-54a1 1 0 0 0-1 1v2.8c0 1.2-1 2.2-2.2 2.2-1.2 0-2.2-1-2.2-2.2v-2.8c0-3 2.5-5.5 5.5-5.5h54c3 0 5.5 2.4 5.5 5.5v2.8c-.2 1.2-1.2 2.2-2.4 2.2z"
                      ></path>
                      <path
                        fill="#F90"
                        d="M63.9 31.4c-1 0-1.9-.7-2.2-1.7L56.6 6.2c-.1-.4-.3-.5-.4-.6-.1-.1-.4-.2-.8-.1L41 8.6c-1.2.3-2.4-.5-2.6-1.7-.3-1.2.5-2.4 1.7-2.6l14.4-3.1c1.4-.3 2.9 0 4.1.7 1.2.8 2.1 2 2.4 3.4l5.1 23.5c.3 1.2-.5 2.4-1.7 2.6h-.5zm-3.7 38.7c-.8 0-1.5-.4-1.9-1.1-3.9-6.7-12.4-14.5-23.8-14.5s-20 7.8-23.8 14.5c-.6 1.1-2 1.4-3 .8-1.1-.6-1.4-2-.8-3 5.9-10.1 16.7-16.7 27.7-16.7s21.8 6.5 27.7 16.7c.6 1.1.3 2.4-.8 3-.5.2-.9.3-1.3.3z"
                      ></path>
                      <ellipse
                        fill="#F90"
                        cx="24.3"
                        cy="63.9"
                        rx="1.7"
                        ry="1.7"
                      ></ellipse>
                      <ellipse
                        fill="#F90"
                        cx="40.7"
                        cy="65.6"
                        rx="1.7"
                        ry="1.7"
                      ></ellipse>
                      <ellipse
                        fill="#F90"
                        cx="33.6"
                        cy="58.8"
                        rx="1.7"
                        ry="1.7"
                      ></ellipse>
                      <path
                        fill="#F90"
                        d="M47 49.6c-1.2 0-2.2-.9-2.2-2.1l-.5-10.3c-.1-1.2.9-2.3 2.1-2.3 1.3-.1 2.3.9 2.3 2.1l.5 10.3c.1 1.2-.9 2.3-2.1 2.3H47zM82.1 93H71.4c-1.2 0-2.2-1-2.2-2.2s1-2.2 2.2-2.2h10.7c.6 0 1-.5 1-1L85.5 37a2.3 2.3 0 0 1 2.3-2.1c1.2.1 2.2 1.1 2.1 2.3l-2.4 50.5c.1 2.9-2.4 5.3-5.4 5.3z"
                      ></path>
                    </svg>
                    <br />
                    <span style={{ fontSize: "10px" }} className="ml-3">
                      Food Available{" "}
                    </span>
                  </div>
                  <div
                    className="col"
                    style={{ backgroundColor: "#FFA07A" }}
                  ></div>
                  <div className="col-12 m-3">
                    {this.state.moviesData.showTiming[day].map((m, index) => (
                      <div className="row mb-4" key={index}>
                        <div
                          className="d-xs-block col-1 "
                          style={{ cursor: "pointer" }}
                        >
                          {m.liked === true ? (
                            <i
                              className="fa fa-heart-o text-danger"
                              aria-hidden="true"
                              onClick={() => this.handleLike(day, index)}
                            />
                          ) : (
                            <i
                              className="fa fa-heart-o"
                              aria-hidden="true"
                              onClick={() => this.handleLike(day, index)}
                            />
                          )}
                        </div>
                        <div
                          className="d-xs-block col-11 col-lg-4"
                          style={{ fontSize: "14px", fontWeight: "500" }}
                        >
                          <span>{m.name}</span>
                          <br />
                          <svg
                            version="1.1"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns={"http://www.w3.org/1999/xlink"}
                            width="20"
                            height="25"
                            viewBox="0 0 130 45"
                            xml={"preserve"}
                            style={{ fill: "green" }}
                          >
                            <path d="M73.5 95.2H26.8c-1.3 0-2.3-1-2.3-2.3V7.6c0-1.3 1-2.3 2.3-2.3h46.7c1.3 0 2.3 1 2.3 2.3V93c0 1.2-1.1 2.2-2.3 2.2zM26.8 6.4c-.6 0-1.1.5-1.1 1.1v85.4c0 .6.5 1.1 1.1 1.1h46.7c.6 0 1.1-.5 1.1-1.1V7.6c0-.6-.5-1.1-1.1-1.1l-46.7-.1z"></path>
                            <path d="M68.8 78.9H31.3c-.6 0-1.2-.5-1.2-1.2V16c0-.6.5-1.2 1.2-1.2h37.5c.6 0 1.2.5 1.2 1.2v61.7c0 .6-.5 1.2-1.2 1.2zm0-62.9H31.3v61.6h37.4l.1-61.6zm-19 74.4c-2.3 0-4.1-1.9-4.1-4.1 0-2.3 1.9-4.1 4.1-4.1 2.3 0 4.1 1.9 4.1 4.1a4 4 0 0 1-4.1 4.1zm0-7.1a2.9 2.9 0 1 0 2.9 2.9c.1-1.5-1.2-2.9-2.9-2.9zm-5.5-72.1h-1.6c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h1.6c.3 0 .6.3.6.6s-.3.6-.6.6zm11 0h-6.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h6.5c.3 0 .6.3.6.6s-.2.6-.6.6z"></path>
                          </svg>
                          <span style={{ fontSize: "9px", marginTop: "10px" }}>
                            M-Ticket
                          </span>
                          <svg
                            version="1.1"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns={"http://www.w3.org/1999/xlink"}
                            width="20"
                            height="25"
                            viewBox="0 0 102 102"
                            xml={"preserve"}
                            style={{ fill: "green" }}
                          >
                            <path
                              fill="#F90"
                              d="M58.7 85.9H8.5c-4.1 0-7.5-3.4-7.5-7.5v-1.1c0-4.1 3.4-7.5 7.5-7.5h50.2c4.1 0 7.5 3.4 7.5 7.5v1.1c0 4.1-3.4 7.5-7.5 7.5zM8.5 74.3a3 3 0 0 0-3 3v1.1a3 3 0 0 0 3 3h50.2a3 3 0 0 0 3-3v-1.1a3 3 0 0 0-3-3H8.5zM54.8 99H12.4c-3.7 0-6.8-3-6.8-6.8v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c0 1.3 1.1 2.3 2.4 2.3h42.4c1.3 0 2.3-1.1 2.3-2.3v-4c0-1.2 1-2.2 2.2-2.2s2.2 1 2.2 2.2v4c.1 3.8-3 6.8-6.7 6.8zm42-57.9c-1.2 0-2.2-1-2.2-2.2v-2.8c0-.6-.5-1-1-1h-54a1 1 0 0 0-1 1v2.8c0 1.2-1 2.2-2.2 2.2-1.2 0-2.2-1-2.2-2.2v-2.8c0-3 2.5-5.5 5.5-5.5h54c3 0 5.5 2.4 5.5 5.5v2.8c-.2 1.2-1.2 2.2-2.4 2.2z"
                            ></path>
                            <path
                              fill="#F90"
                              d="M63.9 31.4c-1 0-1.9-.7-2.2-1.7L56.6 6.2c-.1-.4-.3-.5-.4-.6-.1-.1-.4-.2-.8-.1L41 8.6c-1.2.3-2.4-.5-2.6-1.7-.3-1.2.5-2.4 1.7-2.6l14.4-3.1c1.4-.3 2.9 0 4.1.7 1.2.8 2.1 2 2.4 3.4l5.1 23.5c.3 1.2-.5 2.4-1.7 2.6h-.5zm-3.7 38.7c-.8 0-1.5-.4-1.9-1.1-3.9-6.7-12.4-14.5-23.8-14.5s-20 7.8-23.8 14.5c-.6 1.1-2 1.4-3 .8-1.1-.6-1.4-2-.8-3 5.9-10.1 16.7-16.7 27.7-16.7s21.8 6.5 27.7 16.7c.6 1.1.3 2.4-.8 3-.5.2-.9.3-1.3.3z"
                            ></path>
                            <ellipse
                              fill="#F90"
                              cx="24.3"
                              cy="63.9"
                              rx="1.7"
                              ry="1.7"
                            ></ellipse>
                            <ellipse
                              fill="#F90"
                              cx="40.7"
                              cy="65.6"
                              rx="1.7"
                              ry="1.7"
                            ></ellipse>
                            <ellipse
                              fill="#F90"
                              cx="33.6"
                              cy="58.8"
                              rx="1.7"
                              ry="1.7"
                            ></ellipse>
                            <path
                              fill="#F90"
                              d="M47 49.6c-1.2 0-2.2-.9-2.2-2.1l-.5-10.3c-.1-1.2.9-2.3 2.1-2.3 1.3-.1 2.3.9 2.3 2.1l.5 10.3c.1 1.2-.9 2.3-2.1 2.3H47zM82.1 93H71.4c-1.2 0-2.2-1-2.2-2.2s1-2.2 2.2-2.2h10.7c.6 0 1-.5 1-1L85.5 37a2.3 2.3 0 0 1 2.3-2.1c1.2.1 2.2 1.1 2.1 2.3l-2.4 50.5c.1 2.9-2.4 5.3-5.4 5.3z"
                            ></path>
                          </svg>
                          <span style={{ fontSize: "9px" }}>{"F&B"}</span>
                        </div>
                        <div className="d-xs-block col-12 col-lg-7 ">
                          <div className="row ml-2">
                            {m.timings.map((t, ind) => (
                              <div key={ind} className="">
                                <Link
                                  to={`/bookMovie/${city}/${id}/buyTicket/${index}/${ind}/${day}?time=${date}`}
                                >
                                  <button
                                    className="btn text-primary border border-dark btn-sm m-1"
                                    style={{
                                      fontSize: "",
                                      cursor: "pointer",
                                      fontSize: "11px"
                                    }}
                                  >
                                    {t.name}
                                  </button>
                                </Link>
                              </div>
                            ))}
                          </div>
                          <div
                            className="text-left"
                            style={{ fontSize: "12px" }}
                          >
                            {" "}
                            <span className="text-danger">*</span>{" "}
                            <span>Cancellation Available</span>
                          </div>
                        </div>
                        <br />
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className="d-none d-lg-block col-2">
                <img src={"https://i.ibb.co/JqbbCJz/1331654202504679967.jpg"} />
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Movie;
