import React, { Component } from "react";

class PaymentPage extends Component {
  state = {
    allData: this.props.allData
  };

  handleSave = () => {
    let path = "/bookMovie/";
    path +=
      this.state.allData.city +
      "/" +
      this.state.allData.movieId +
      "/buyTicket/" +
      this.state.allData.cId +
      "/" +
      this.state.allData.timeId +
      "/" +
      this.state.allData.dateId +
      "?time=" +
      this.state.allData.time;
    this.props.history.push(path);
  };
  render() {
    return (
      <div>
        <div>
          <div className="bg-dark ">
            <div className="row text-white">
              <div className="d-xs-block col-12 col-lg-6  mt-1">
                <span
                  className="ml-3"
                  onClick={() => this.handleSave()}
                  style={{ cursor: "pointer", fontSize: "14px" }}
                >
                  <i className="fa fa-chevron-left" aria-hidden="true" />
                </span>
                <span className="ml-4" style={{ fontSize: "25px" }}>
                  {this.state.allData.movie}
                </span>
                <br />
                <span
                  className="ml-5"
                  style={{ fontSize: "13px", margin: "10px" }}
                >
                  {this.state.allData.hall}
                </span>
              </div>
              <div className=" d-none d-lg-block  col-lg-6  mt-4 text-right ">
                <span
                  className="mr-3"
                  onClick={() => this.handleSave()}
                  style={{ cursor: "pointer" }}
                >
                  <i className="fa fa-times" aria-hidden="true" />
                </span>
              </div>
            </div>
            <br />
          </div>
          <div className="row bg-light m-1">
            <div className="d-xs-block col-12 col-md-5 col-lg-8 bg-white border border-light m-1">
              <img
                src={"https://i.ibb.co/SK0HfNT/bookasmile-03.png"}
                style={{ width: "100%" }}
              />
            </div>
            <div className="d-xs-block col-12 col-md-6 col-lg-3 bg-white m-1">
              <h6 className="text-danger">BOOKING SUMMARY</h6>
              <br />
              <div className="row">
                <div className="col-6 text-left">Movie Name</div>
                <div className="col-6 text-rigth">
                  {this.state.allData.movie}
                </div>
                <div className="col-6 text-left">Movie Hall</div>
                <div className="col-6 text-rigth">
                  {this.state.allData.hall}
                </div>
                <div className="col-6 text-left">Total Tickets</div>
                <div className="col-6 text-rigth">
                  {this.state.allData.total}
                </div>
                <div className="col-6 text-left">Tickets</div>
                <div className="col-6 text-rigth">
                  {this.state.allData.tickets}
                </div>
                <div className="col-6 text-left">Date</div>
                <div className="col-6 text-rigth">
                  {this.state.allData.date}
                </div>
                <div className="col-6 text-left">Time</div>
                <div className="col-6 text-rigth">
                  {this.state.allData.time}
                </div>
                <div
                  className="col-6 text-left"
                  style={{ backgroundColor: "#FCF3CF" }}
                >
                  Amount Paid
                </div>
                <div
                  className="col-6 text-rigth"
                  style={{ backgroundColor: "#FCF3CF" }}
                >
                  {this.state.allData.amount}
                </div>
                <div className="col-12 m-1">
                  <img
                    src={
                      "https://i.ibb.co/CVHYxVK/images-q-tbn-ANd9-Gc-Qq-PT1-GB7-Cpvo3-WDDCi-Wt-Vto-Q-SLqp-Z9-B1x-D3-D69-WTj-MPyl-Chnd.png"
                    }
                    style={{ width: "100%" }}
                  />
                  <span style={{ fontSize: "9px" }}>
                    You can cancel the tickets 4 hour(s) before the show.
                    Refunds will be done according to Cancellation Policy.
                  </span>
                </div>
              </div>
            </div>
            <div className="col-1 bg-light"></div>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentPage;
