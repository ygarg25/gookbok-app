import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "./front.css";
import queryString from "query-string";

let languageCheckBox = [];
let formatCheckBox = [];
let genreCheckBox = [];
class FrontScreen extends Component {
  state = {
    languageData: ["Hindi", "English", "Punjabi", "Tamil"],
    formatData: ["2D", "3D", "4DX"],
    genreData: ["Action", "Adventure", "Biography", "Comedy"],
    count: 1,
    langTrue: true,
    formatTrue: true,
    genreTrue: true
  };

  makeCbStructure(mainData, selectedData) {
    let temp = mainData.map(n1 => ({
      name: n1,
      isSelected: false
    }));
    let cbSelectedData = selectedData.split(",");
    for (let i = 0; i < cbSelectedData.length; i++) {
      let obj = temp.find(n1 => n1.name === cbSelectedData[i]);
      if (obj) obj.isSelected = true;
    }
    return temp;
  }

  handleChange = e => {
    const { currentTarget: input } = e;
    let { q, lang, format, genre } = queryString.parse(
      this.props.location.search
    );
    if (input.type === "checkbox") {
      if (input.id === "language") {
        let cb = languageCheckBox.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredlanguage = languageCheckBox.filter(n1 => n1.isSelected);
        let arraylanguage = filteredlanguage.map(n1 => n1.name);
        let sellang = arraylanguage.join(",");
        lang = sellang;
      } else if (input.id === "format") {
        let cb = formatCheckBox.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredFormat = formatCheckBox.filter(n1 => n1.isSelected);
        let arrayFormat = filteredFormat.map(n1 => n1.name);
        let selformat = arrayFormat.join(",");
        format = selformat;
      } else if (input.id === "genre") {
        let cb = genreCheckBox.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredGenre = genreCheckBox.filter(n1 => n1.isSelected);
        let arrayGenre = filteredGenre.map(n1 => n1.name);
        let selGenre = arrayGenre.join(",");
        genre = selGenre;
      }
    }

    this.callUrl("", q, lang, format, genre);
  };

  callUrl = (params, q, lang, format, genre) => {
    const city = this.props.match.params.city;
    let path = "/home";
    if (city) path = path + "/" + city;
    params = this.addToParams(params, "q", q);
    params = this.addToParams(params, "lang", lang);
    params = this.addToParams(params, "format", format);
    params = this.addToParams(params, "genre", genre);
    this.props.history.push({ pathname: path, search: params });
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  async componentWillMount() {
    const city = this.props.match.params.city;
    let { q, lang, format, genre } = queryString.parse(
      this.props.location.search
    );
    let params = "";
    let apiEndPint =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/movies/";

    apiEndPint = apiEndPint + city;

    params = this.addToParams(params, "q", q);
    params = this.addToParams(params, "lang", lang);
    params = this.addToParams(params, "format", format);
    params = this.addToParams(params, "genre", genre);

    apiEndPint = apiEndPint + params;
    const { data: moviesData } = await axios.get(apiEndPint);
    this.setState({ moviesData: moviesData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    const city = this.props.match.params.city;
    let { q, lang, format, genre } = queryString.parse(
      this.props.location.search
    );
    let params = "";

    let apiEndPint =
      "https://us-central1-bkyow-22da6.cloudfunctions.net/app/movies/";
    apiEndPint = apiEndPint + city;
    params = this.addToParams(params, "q", q);
    params = this.addToParams(params, "lang", lang);
    params = this.addToParams(params, "format", format);
    params = this.addToParams(params, "genre", genre);
    apiEndPint = apiEndPint + params;
    if (currProps !== prevProps) {
      const { data: moviesData } = await axios.get(apiEndPint);
      this.setState({ moviesData: moviesData });
    }
  }
  handledropDown = key => {
    if (key === "langTrue") {
      this.setState({ langTrue: !this.state.langTrue });
    }
    if (key === "formatTrue") {
      this.setState({ formatTrue: !this.state.formatTrue });
    }
    if (key === "genreTrue") {
      this.setState({ genreTrue: !this.state.genreTrue });
    }
  };
  render() {
    const city = this.props.match.params.city;
    let { q, lang, format, genre } = queryString.parse(
      this.props.location.search
    );
    const { langTrue, formatTrue, genreTrue } = this.state;
    lang = lang ? lang : "";
    format = format ? format : "";
    genre = genre ? genre : "";
    languageCheckBox = this.makeCbStructure(this.state.languageData, lang);
    formatCheckBox = this.makeCbStructure(this.state.formatData, format);
    genreCheckBox = this.makeCbStructure(this.state.genreData, genre);
    return (
      <div>
        <div
          className="container pt-3"
          style={{ width: "67%", height: "auto" }}
        >
          <div id="carouselExampleIndicators" className="carousel slide">
            <ol className="carousel-indicators">
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="0"
                className="active"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="1"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="2"
              ></li>
            </ol>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img
                  className="d-block w-100"
                  src={
                    "https://i.ibb.co/ZGsJ3dh/jio-mami-21st-mumbai-film-festival-with-star-2019-02-09-2019-10-58-45-992.png"
                  }
                  alt="First slide"
                />
              </div>
              <div className="carousel-item">
                <img
                  className="d-block w-100"
                  src={
                    "https://i.ibb.co/wRr7W1P/hustlers-01-10-2019-05-09-55-486.png"
                  }
                  alt="Second slide"
                />
              </div>
              <div className="carousel-item">
                <img
                  className="d-block w-100"
                  src={
                    "https://i.ibb.co/qFWPRpF/laal-kaptaan-16-10-2019-12-48-06-721.jpg"
                  }
                  alt="Third slide"
                />
              </div>
            </div>
            <a
              className="carousel-control-prev"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="carousel-control-next"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
        <div className="d-none d-lg-block ml-1 mr-1">
          <div className=" container-fluid ml-1 bg-light row p-2">
            <span
              style={{
                cursor: "pointer",
                textDecoration: "bold",
                fontSize: "23px"
              }}
              className="mr-4  ml-2 pt-2 text-dark"
            >
              Movies
            </span>
            <span
              style={{
                cursor: "pointer",
                fontSize: "14px",
                verticalAlign: "bottom"
              }}
              className="pt-3 btncss"
            >
              <a>Now Showing</a>
            </span>
            <span
              style={{
                cursor: "pointer",
                fontSize: "14px",
                verticalAlign: "bottom"
              }}
              className="pt-3 ml-4 btncss"
            >
              <a>Coming Soon</a>
            </span>
            <span
              style={{
                cursor: "pointer",
                fontSize: "14px",
                verticalAlign: "bottom"
              }}
              className="pt-3 ml-4 btncss"
            >
              <a>Exclusive</a>
            </span>
          </div>
        </div>
        <div className="bg-light row">
          <div className="d-none d-lg-block col-3 text-center ml-3">
            <div className=" bg-white ">
              <img
                src={"https://i.ibb.co/Hry1kDH/17443322900502723126.jpg"}
                style={{ width: "80%", height: "auto" }}
                className="m-2 "
              />
            </div>
            <br />
            <br />
            {langTrue ? (
              <div
                className="col-12 bg-white text-dark"
                style={{ fontSize: "16px" }}
              >
                <div className="row m-1 p-1">
                  <span
                    className="text-left"
                    onClick={() => this.handledropDown("langTrue")}
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-chevron-down " />
                  </span>
                  <span className="ml-4 text-left">Select Language</span>
                </div>
              </div>
            ) : (
              <div
                className="col-12 bg-white text-dark"
                style={{ fontSize: "16px" }}
              >
                <div className="row m-1 p-1 text-primary">
                  <span
                    className="text-left"
                    onClick={() => this.handledropDown("langTrue")}
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-chevron-up" />
                  </span>
                  <span className="ml-4 text-left">Select Language</span>
                </div>
                <div className=" ml-3">
                  {languageCheckBox.map((l, index) => (
                    <div className="row" key={index}>
                      <div className=" col-12 form-check text-left m-1 ml-2">
                        <input
                          value={l.isSelected}
                          onChange={this.handleChange}
                          type="checkbox"
                          id="language"
                          name={l.name}
                          checked={l.isSelected}
                          className="form-check-input"
                        />
                        <label className="form-check-label" htmlFor={l.name}>
                          {l.name}
                        </label>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}
            <br />
            {formatTrue ? (
              <div
                className="col-12 bg-white text-dark"
                style={{ fontSize: "16px" }}
              >
                <div className="row m-1 p-1">
                  <span
                    className="text-left"
                    onClick={() => this.handledropDown("formatTrue")}
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-chevron-down " />
                  </span>
                  <span className="ml-4 text-left">Format</span>
                </div>
              </div>
            ) : (
              <div
                className="col-12 bg-white text-dark"
                style={{ fontSize: "16px" }}
              >
                <div className="row m-1 p-1 text-primary">
                  <span
                    className="text-left"
                    onClick={() => this.handledropDown("formatTrue")}
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-chevron-up" />
                  </span>
                  <span className="ml-4 text-left">Format</span>
                </div>
                <div className=" ml-3">
                  {formatCheckBox.map((l, index) => (
                    <div className="row" key={index}>
                      <div className=" col-12 form-check text-left m-1 ml-2">
                        <input
                          value={l.isSelected}
                          onChange={this.handleChange}
                          type="checkbox"
                          id="format"
                          name={l.name}
                          checked={l.isSelected}
                          className="form-check-input"
                        />
                        <label className="form-check-label" htmlFor={l.name}>
                          {l.name}
                        </label>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}

            <br />
            {genreTrue ? (
              <div
                className="col-12 bg-white text-dark"
                style={{ fontSize: "16px" }}
              >
                <div className="row m-1 p-1">
                  <span
                    className="text-left"
                    onClick={() => this.handledropDown("genreTrue")}
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-chevron-down " />
                  </span>
                  <span className="ml-4 text-left">Genre</span>
                </div>
              </div>
            ) : (
              <div
                className="col-12 bg-white text-dark"
                style={{ fontSize: "16px" }}
              >
                <div className="row m-1 p-1 text-primary">
                  <span
                    className="text-left"
                    onClick={() => this.handledropDown("genreTrue")}
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-chevron-up" />
                  </span>
                  <span className="ml-4 text-left">Genre</span>
                </div>
                <div className=" ml-3">
                  {genreCheckBox.map((l, index) => (
                    <div className="row" key={index}>
                      <div className=" col-12 form-check text-left m-1 ml-2">
                        <input
                          value={l.isSelected}
                          onChange={this.handleChange}
                          type="checkbox"
                          id="genre"
                          name={l.name}
                          checked={l.isSelected}
                          className="form-check-input"
                        />
                        <label className="form-check-label" htmlFor={l.name}>
                          {l.name}
                        </label>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}
            <br />
            <br />
            <br />
          </div>
          <div className="d-xs-block col-11 col-lg-7 bg-light">
            {this.state.moviesData && (
              <div className="row">
                {this.state.moviesData.map((m, index) => (
                  <div
                    className=" bg-light d-xs-block col-5 col-sm-4 col-md-4 col-lg-4 ml-4 ml-lg-0"
                    key={index}
                  >
                    <div className=" bg-white text-center">
                      <Link to={`/home/${city}/${index}`}>
                        <img
                          className="rounded-top m-1 "
                          src={m.img}
                          style={{ width: "90%", height: "auto", padding: "0" }}
                        />
                      </Link>
                      <div className="row ">
                        <div
                          className="d-xs-block col-12 col-lg-4 m-1 text-left"
                          style={{ fontSize: "14px" }}
                        >
                          <i
                            className="fa fa-heart text-danger"
                            aria-hidden="true"
                          />
                          {m.rating}
                        </div>
                        <div
                          className="d-xs-block col-12 col-lg-7 m-1 text-left text-lg-right"
                          style={{ fontSize: "14px" }}
                        >
                          {m.title}
                        </div>
                      </div>
                      <div className="row">
                        <div
                          className="d-none d-lg-block col-6 text-left"
                          style={{ fontSize: "11px" }}
                        >
                          {m.votes} Votes
                        </div>
                        <div
                          className="d-none d-lg-block  col-6 text-right"
                          style={{ fontSize: "11px" }}
                        >
                          {m.desc}
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
          <div className="col-2"></div>
        </div>
      </div>
    );
  }
}

export default FrontScreen;
