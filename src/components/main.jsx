import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import NavBar from "./navbar";
import Home from "./home";
import Books from "./books";

class Main extends Component {
  state = {};
  render() {
    return (
      <div className="container-fluid">
        <NavBar />
        <div>
          <Switch>
            <Route path="/books" component={Books} />
            <Route path="/" component={Home} />
            <Redirect to="/" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Main;
