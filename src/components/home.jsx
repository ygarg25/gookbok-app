import React, { Component } from "react";
import { Link } from "react-router-dom";
import Books from "./books";

class Home extends Component {
  state = {
    formdata: { search: "" }
  };

  handleSubmit = e => {
    e.preventDefault();
    console.log("The Course name is ", this.state.formdata.search);
  };

  handleChange = e => {
    const data = { ...this.state.formdata };
    data[e.currentTarget.name] = e.currentTarget.value;
    this.setState({ formdata: data });
  };

  render() {
    return (
      <div className="container">
        <br />
        <br />
        <div className="row">
          <div className="col-4"></div>
          <div className="Col-6">
            <img
              src={require("./frontImage.jpg")}
              className="mb-2"
              alt="Avatar"
              style={{ borderRadius: "50%" }}
            />
          </div>
          <div className="col-2"></div>
        </div>
        <div className="row">
          <div className="col-1"></div>
          <div className="col-9">
            <form className="row" onSubmit={this.handleSubmit}>
              <div className="form-group col-9">
                <input
                  type="text"
                  className="form-control"
                  id="search"
                  name="search"
                  placeholder="Search"
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-3">
                <Link
                  to={`/books?searchText=${this.state.formdata.search}&startIndex=0&maxResults=8`}
                >
                  <button type="submit" className="btn btn-primary">
                    Search
                  </button>
                </Link>
              </div>
            </form>
          </div>
          <div className="col-2"></div>
        </div>
      </div>
    );
  }
}

export default Home;
