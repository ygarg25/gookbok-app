import React, { Component } from "react";

class LeftPanelForm extends Component {
  handleChange = e => {
    let checkboxname = "";
    const { currentTarget: input } = e;
    const { FilterCheckbox, languageCheckbox } = this.props;
    if (input.type === "checkbox") {
      if (input.id === "filter") {
        let cb = FilterCheckbox.find(n1 => n1.refineValue === input.name);
        checkboxname = "filter";
        if (cb) cb.isSelected = input.checked;
      } else if (input.id === "language") {
        let cb = languageCheckbox.find(n1 => n1.refineValue === input.name);
        checkboxname = "language";
        if (cb) cb.isSelected = input.checked;
      }
    }
    console.log("handlechange");
    this.props.onOptChange(FilterCheckbox, languageCheckbox, checkboxname);
  };
  render() {
    const { FilterCheckbox, languageCheckbox } = this.props;
    console.log("filterCheckbox", FilterCheckbox);
    return (
      <div>
        <form>
          <div className="text-left col-12 ">
            <div className="border bg-light col-12">Languages</div>
            {languageCheckbox.map((item, index) => (
              <div className="form-check border" key={index}>
                <input
                  value={item.isSelected}
                  onChange={this.handleChange}
                  id="language"
                  type="checkbox"
                  name={item.refineValue}
                  checked={item.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={item.refineValue}>
                  {item.refineValue}
                </label>
              </div>
            ))}
          </div>
          <br />
          <hr className="myhr4" />
          <br />
          <div className="text-left col-12 ">
            <div className="border bg-light col-12">Filter</div>
            {FilterCheckbox.map((item, index) => (
              <div className="form-check border" key={index}>
                <input
                  value={item.isSelected}
                  onChange={this.handleChange}
                  id="filter"
                  type="checkbox"
                  name={item.refineValue}
                  checked={item.isSelected}
                  className="form-check-input"
                />
                <label className="form-check-label" htmlFor={item.refineValue}>
                  {item.refineValue}
                </label>
              </div>
            ))}
          </div>
        </form>
      </div>
    );
  }
}

export default LeftPanelForm;
