import React, { Component } from "react";
import LeftPanelForm from "./form1";
import axios from "axios";
import queryString from "query-string";

class Books extends Component {
  state = {
    books: {},
    language: [
      { lang: "English", code: "en" },
      { lang: "French", code: "fr" },
      { lang: "Hindi", code: "hi" }
    ],
    Filter: [
      { name: "Full Volume", code: "full" },
      { name: "Free Google e-books", code: "free-ebooks" },
      { name: "Paid Google e-books", code: "paid-ebooks" }
    ],
    count: 0
  };

  async componentWillMount() {
    let {
      searchText,
      startIndex,
      maxResults,
      filter,
      langRestrict
    } = queryString.parse(this.props.location.search);
    startIndex = startIndex ? startIndex : 0;
    maxResults = 8;

    let params = "";

    let apiEndPint = "https://www.googleapis.com/books/v1/volumes";
    if (searchText) {
      let arr = searchText.split(" ");
      let arr1 = arr.join("%20");
      apiEndPint = apiEndPint + "?q=" + arr1;
      if (langRestrict)
        apiEndPint = apiEndPint + "&langRestrict=" + langRestrict;
      if (filter) apiEndPint = apiEndPint + "&filter=" + filter;
      if (startIndex) apiEndPint = apiEndPint + "&startIndex=" + startIndex;
      if (maxResults) apiEndPint = apiEndPint + "&maxResults=" + maxResults;
    }
    params = this.addToParams(params, "searchText", searchText);
    params = this.addToParams(params, "langRestrict", langRestrict);
    params = this.addToParams(params, "filter", filter);
    params = this.addToParams(params, "startIndex", startIndex);
    params = this.addToParams(params, "maxResults", maxResults);

    const { data: books } = await axios.get(apiEndPint);
    this.setState({ books: books });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    let {
      searchText,
      startIndex,
      maxResults,
      filter,
      langRestrict
    } = queryString.parse(this.props.location.search);
    startIndex = startIndex ? startIndex : 0;
    maxResults = 8;

    let params = "";

    let apiEndPint = "https://www.googleapis.com/books/v1/volumes";
    if (currProps !== prevProps) {
      console.log("check prevProps and currProps", currProps === prevProps);
      if (searchText) {
        let arr = searchText.split(" ");
        let arr1 = arr.join("%20");
        apiEndPint = apiEndPint + "?q=" + arr1;
        if (langRestrict)
          apiEndPint = apiEndPint + "&langRestrict=" + langRestrict;
        if (filter) apiEndPint = apiEndPint + "&filter=" + filter;
        if (startIndex) apiEndPint = apiEndPint + "&startIndex=" + startIndex;
        if (maxResults) apiEndPint = apiEndPint + "&maxResults=" + maxResults;
      }
      params = this.addToParams(params, "searchText", searchText);
      params = this.addToParams(params, "filter", filter);
      params = this.addToParams(params, "startIndex", startIndex);
      params = this.addToParams(params, "maxResults", maxResults);

      const { data: books } = await axios.get(apiEndPint);
      this.setState({ books: books });
    }
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue !== undefined) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callUrl = (
    params,
    searchText,
    startIndex,
    maxResults,
    filter,
    langRestrict
  ) => {
    let path = "/books";
    params = this.addToParams(params, "searchText", searchText);
    params = this.addToParams(params, "filter", filter);
    params = this.addToParams(params, "langRestrict", langRestrict);
    params = this.addToParams(params, "startIndex", startIndex);
    params = this.addToParams(params, "maxResult", maxResults);
    this.props.history.push({ pathname: path, search: params });
  };

  gotoPage = x => {
    let {
      searchText,
      startIndex,
      maxResults,
      filter,
      langRestrict
    } = queryString.parse(this.props.location.search);
    let startindex = startIndex ? +startIndex : 0;
    this.state.count += x;
    startindex = this.state.count * 8;

    let params = "";
    this.callUrl(params, searchText, startindex, 8, filter, langRestrict);
  };

  makeCbStructure(FilterData, filter) {
    let temp = FilterData.map(n1 => ({
      refineValue: n1.name,
      code: n1.code,
      isSelected: false
    }));
    let cbFilterData = filter.split(",");
    for (let i = 0; i < cbFilterData.length; i++) {
      let obj = temp.find(n1 => n1.code === cbFilterData[i]);
      if (obj) obj.isSelected = true;
    }
    return temp;
  }

  makeCbStructure1(languageData, langRestrict) {
    let temp = languageData.map(n1 => ({
      refineValue: n1.lang,
      code: n1.code,
      isSelected: false
    }));
    let cblanguageData = langRestrict.split(",");
    for (let i = 0; i < cblanguageData.length; i++) {
      let obj = temp.find(n1 => n1.code === cblanguageData[i]);
      if (obj) obj.isSelected = true;
    }
    return temp;
  }

  handleOptionChange = (filterCheckbox, languageCheckbox, checkboxname) => {
    let {
      searchText,
      startIndex,
      maxResults,
      filter,
      langRestrict
    } = queryString.parse(this.props.location.search);
    if (checkboxname === "filter") {
      let filteredFilter = filterCheckbox.filter(n1 => n1.isSelected);
      let arrayFilter = filteredFilter.map(n1 => n1.code);
      let selFilter = arrayFilter.join(",");
      filter = selFilter;
    }
    if (checkboxname === "language") {
      let filteredLanguage = languageCheckbox.filter(n1 => n1.isSelected);
      let arraylanguage = filteredLanguage.map(n1 => n1.code);
      langRestrict = arraylanguage.join(",");
    }

    startIndex = 0;
    this.state.count = 0;
    this.callUrl("", searchText, startIndex, 8, filter, langRestrict);
  };

  render() {
    let {
      searchText,
      startIndex,
      maxResults,
      filter,
      langRestrict
    } = queryString.parse(this.props.location.search);

    startIndex = startIndex ? startIndex : 0;
    filter = filter ? filter : "";
    langRestrict = langRestrict ? langRestrict : "";

    const tableData = this.state.books.items;
    let pageInfo = { ...this.state.books.pageInfo };

    let FilterData = this.state.Filter;
    let languageData = this.state.language;
    let FilterCheckbox = this.makeCbStructure(FilterData, filter);
    let languageCheckbox = this.makeCbStructure1(languageData, langRestrict);

    return (
      <div>
        <br />
        <div className="row">
          <div className="col-3">
            <LeftPanelForm
              FilterCheckbox={FilterCheckbox}
              languageCheckbox={languageCheckbox}
              onOptChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">
            <div className="row">
              <h3 className="text-warning text-center col-12">All Books</h3>
            </div>
            <div>
              <div className="text-primary">
                {+startIndex + 1}
                <span> - </span>
                <span>{+startIndex + 8}</span>
                <span>entries</span>
              </div>
            </div>
            {tableData !== undefined ? (
              <div>
                {" "}
                <div className="row">
                  {tableData.map((b1, index) => (
                    <div
                      className="col-3 text-center border bg-success"
                      key={index}
                    >
                      {b1.volumeInfo.imageLinks.smallThumbnail !== undefined ? (
                        <img
                          src={b1.volumeInfo.imageLinks.smallThumbnail}
                          style={{ width: "60%", height: "50%" }}
                        />
                      ) : (
                        ""
                      )}
                      <br />
                      <h5> {b1.volumeInfo.title}</h5>
                      <h6>
                        <div className="row">
                          {b1.volumeInfo.authors !== undefined && (
                            <div className="col-12 text-center">
                              by
                              {b1.volumeInfo.authors.map((b1, index) => (
                                <div className="col-12 text-center" key={index}>
                                  <span className="">{b1} </span>
                                </div>
                              ))}
                            </div>
                          )}
                        </div>
                      </h6>
                      <h6>
                        {b1.volumeInfo.categories !== undefined ? (
                          <span>{b1.volumeInfo.categories}</span>
                        ) : (
                          "NA"
                        )}
                      </h6>
                    </div>
                  ))}
                </div>
              </div>
            ) : (
              ""
            )}
            <br />
            <div className="row">
              <div className="col-6 text-left">
                {startIndex > 0 ? (
                  <button
                    className="btn btn-warning"
                    onClick={() => this.gotoPage(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-6">
                {+startIndex === 0 ? (
                  <div className="text-left">
                    <button
                      className="btn btn-warning"
                      onClick={() => this.gotoPage(1)}
                    >
                      Next
                    </button>
                  </div>
                ) : (
                  <div className="text-right">
                    <button
                      className="btn btn-warning"
                      onClick={() => this.gotoPage(1)}
                    >
                      Next
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Books;
