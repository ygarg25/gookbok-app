import React from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        <i className="fa fa-book fa-lg" aria-hidden="true"></i>
      </Link>
      <div className="" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item dropdown">
            <Link
              className="nav-link dropdown-toggle"
              to="#"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Options
            </Link>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link
                className="dropdown-item"
                to={`/books?searchText=Harry Potter&startIndex=0&maxResults=8`}
              >
                Harry Potter Books
              </Link>
              <Link
                className="dropdown-item"
                to={`/books?searchText=Agatha Christie&startIndex=0&maxResults=8`}
              >
                Books by Agatha Christie
              </Link>
              <Link
                className="dropdown-item"
                to={`/books?searchText=Premchand&startIndex=0&maxResults=8`}
              >
                Books by Premchand
              </Link>
              <Link
                className="dropdown-item"
                to={`/books?searchText=Love Stories by Jane&startIndex=0&maxResults=8`}
              >
                Love Stories by Jane
              </Link>
              <Link
                className="dropdown-item"
                to={`/books?searchText=Biography on Lincoln&startIndex=0&maxResults=8`}
              >
                Biography on Lincoln
              </Link>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
