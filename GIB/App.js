import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import auth from "./Service/authService";
import "./App.css";
import NavBar from "./component/navbar";
import LoginForm from "./component/loginform";
import NotFound from "./component/notfound";
import Admin from "./component/admin";
import AllCustomers from "./component/allcustomer";
import LogOut from "./component/logout";
import AddCustomer from "./component/addCustomer";
import AllCheques from "./component/allCheques";
import AllNetBanking from "./component/allNetBanking";
import ProtectedRoute from "./component/protectedRoute";
import Customer from "./component/customer";
import ViewCheque from "./component/viewCheque";
import ViewNetBanking from "./component/viewNetBanking";
import ChequeTransaction from "./component/chequeTransaction";
import AddPayee from "./component/addPayee";
import NetBAnkingTransaction from "./component/netBankingTransaction";
import CustomerDetail from "./component/customerDetails";
import NomineeDetail from "./component/nomineeDetail";

class App extends Component {
  state = {};

  componentDidMount() {
    const userInfo = auth.getCurrentUser();
    if (userInfo) {
      let data = userInfo.split("-");
      let user = data[0];
      let role = data[1];
      this.setState({ user, role });
    }
  }
  render() {
    const { user, role } = this.state;
    return (
      <div className="">
        <NavBar user={user} role={role} />
        <div>
          <Switch>
            <Route path="/not-found" component={NotFound} />
            <Route path="/login" component={LoginForm} />
            <Route path="/logout" component={LogOut} />
            <Route
              path="/admin"
              render={props => {
                if (!user && role !== "manager")
                  return <Redirect to="/login" />;
                return <Admin {...props} />;
              }}
            />
            {/* <Route path="/allCustomers" component={AllCustomers} /> */}
            <ProtectedRoute path="/allCustomers" component={AllCustomers} />
            <Route path="/addCustomer" component={AddCustomer} />
            <Route path="/allCheques" component={AllCheques} />
            <Route path="/allNet" component={AllNetBanking} />
            <Route path="/customer" component={Customer} />
            <Route path="/viewCheque" component={ViewCheque} />
            <Route path="/viewNet" component={ViewNetBanking} />
            <Route path="/cheque" component={ChequeTransaction} />
            <Route path="/addPayee" component={AddPayee} />
            <Route path="/netBanking" component={NetBAnkingTransaction} />
            <Route path="/customerDetails" component={CustomerDetail} />
            <Route path="/nomineeDetails" component={NomineeDetail} />
            <Redirect to="/login" />
            <Redirect to="/not-found" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
