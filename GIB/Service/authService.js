import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/login";

const tokenKey = "token";

export async function login(name, password) {
  const { data: jwt } = await http.post(apiEndpoint, {
    name: name,
    password: password
  });
  let token = jwt.name + "-" + jwt.role;
  localStorage.setItem(tokenKey, token);
}

// export function loginWithJwt(jwt) {
//   localStorage.setItem(tokenKey, jwt);
// }

export function logout() {
  localStorage.removeItem(tokenKey);
  console.log("logout");
}

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    return jwt;
  } catch (ex) {
    return null;
  }
}

export function addCustomer(name, password) {
  let url = apiUrl + "/register";
  return http.post(url, {
    name: name,
    password: password
  });
}

export function addCheque(name, cNum, bName, branch, amt) {
  let url = apiUrl + "/postCheque";
  return http.post(url, {
    name: name,
    chequeNumber: cNum,
    bankName: bName,
    branch: branch,
    amount: amt
  });
}

export function addPayee(name, pName, ifsc, acc, bName) {
  let url = apiUrl + "/addPayee";
  return http.post(url, {
    name: name,
    payeeName: pName,
    IFSC: ifsc,
    accNumber: acc,
    bankName: bName
  });
}

export function addNetBanking(name, pName, cmt, amt, bName) {
  let url = apiUrl + "/postNet";
  return http.post(url, {
    name: name,
    payeeName: pName,
    comment: cmt,
    amount: amt,
    bankName: bName
  });
}

export function addCustomerDetail(
  name,
  gender,
  adLIne1,
  state,
  city,
  dob,
  PAN
) {
  let url = apiUrl + "/customerDetails";
  return http.post(url, {
    name: name,
    gender: gender,
    addressLine1: adLIne1,
    state: state,
    city: city,
    dob: dob,
    PAN: PAN
  });
}

export function addNomineeDetail(
  name,
  nName,
  gender,
  dob,
  rShip,
  jointSignatory
) {
  let url = apiUrl + "/nomineeDetails";
  return http.post(url, {
    name: name,
    nomineeName: nName,
    gender: gender,
    dob: dob,
    relationship: rShip,
    jointSignatory: jointSignatory
  });
}

export default {
  login,
  getCurrentUser,
  logout,
  addCustomer,
  addCheque,
  addPayee,
  addNetBanking,
  addCustomerDetail,
  addNomineeDetail
};
