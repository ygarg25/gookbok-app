import React, { Component } from "react";
import http from "./../Service/httpService";
import queryString from "query-string";

class AllCustomers extends Component {
  state = {
    custData: {}
  };

  async componentWillMount() {
    //const task = this.props.match.params.task;
    let { page } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    let apiEndPint = "http://localhost:2450/getCustomers";

    params = this.addToParams(params, "page", page);
    apiEndPint = apiEndPint + params;
    const { data: custData } = await http.get(apiEndPint);
    this.setState({ custData: custData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;

    let { page } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    let apiEndPint = "http://localhost:2450/getCustomers";

    params = this.addToParams(params, "page", page);
    apiEndPint = apiEndPint + params;

    if (currProps !== prevProps) {
      console.log("check prevProps and currProps", currProps === prevProps);
      const { data: custData } = await http.get(apiEndPint);
      this.setState({ custData: custData });
    }
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callUrl = (params, page) => {
    let path = "/allCustomers";
    params = this.addToParams(params, "page", page);
    this.props.history.push({ pathname: path, search: params });
  };

  gotoPage = x => {
    let { page } = queryString.parse(this.props.location.search);
    let currentPage = page ? +page : 1;
    currentPage = currentPage + x;
    let params = "";
    this.callUrl(params, currentPage);
  };
  render() {
    let { page } = queryString.parse(this.props.location.search);
    page = page ? page : 1;
    let maxPage;
    let { custData } = { ...this.state.custData };
    let item = this.state.custData.items;
    if (item) {
      maxPage = Math.ceil(this.state.custData.totalNum / 5);
    }
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">All Customers</h3>
        <div>
          <br />

          {item && (
            <div>
              <br />
              {(this.state.custData.page - 1) * 5 + 1} -{" "}
              {item.length === 5 ? (
                <span>{this.state.custData.page * 5}</span>
              ) : (
                <span>{this.state.custData.totalNum}</span>
              )}{" "}
              of {this.state.custData.totalNum}
              <br />
              <div className="row border-top border-bottom">
                <div className="col-2">Name</div>
                <div className="col-3">State</div>
                <div className="col-2">City</div>
                <div className="col-2">PAN</div>
                <div className="col-3">DOB</div>
              </div>
              {item.map((i, index) => (
                <div className="" key={index}>
                  {index % 2 == 0 ? (
                    <div className="row bg-light border-top border-bottom">
                      <div className="col-2">{i.name}</div>
                      <div className="col-3">{i.state}</div>
                      <div className="col-2">{i.city}</div>
                      <div className="col-2">{i.PAN}</div>
                      <div className="col-3">{i.dob}</div>
                    </div>
                  ) : (
                    <div className="row border-top border-bottom">
                      <div className="col-2">{i.name}</div>
                      <div className="col-3">{i.state}</div>
                      <div className="col-2">{i.city}</div>
                      <div className="col-2">{i.PAN}</div>
                      <div className="col-3">{i.dob}</div>
                    </div>
                  )}
                </div>
              ))}
              <br />
              <div className="row">
                <div className="col-6 text-left">
                  {+page > 1 ? (
                    <button
                      className="btn btn-secondary"
                      onClick={() => this.gotoPage(-1)}
                    >
                      Previous
                    </button>
                  ) : (
                    ""
                  )}
                </div>
                <div className="col-6">
                  {page < maxPage ? (
                    <div className="text-right">
                      <button
                        className="btn btn-secondary"
                        onClick={() => this.gotoPage(1)}
                      >
                        Next
                      </button>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default AllCustomers;
