import React, { Component } from "react";
import auth from "./../Service/authService";
import { Redirect } from "react-router-dom";

class LoginForm extends Component {
  state = {
    data: { name: "", password: "" },
    errors: {}
  };

  handleSubmit = async e => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      await auth.login(this.state.data.name, this.state.data.password);

      const { state } = this.props.location;
      console.log("ss", state);
      let path = "/";
      let userInfo = auth.getCurrentUser();
      if (userInfo) {
        let data = userInfo.split("-");
        let role = data[1];
        if (role === "manager") path += "admin";
        if (role === "customer") path += "customer";
      }
      window.location = state ? state.from.pathname : path;
    } catch (ex) {
      if (
        ex.response &&
        ex.response.status >= 400 &&
        ex.response.status <= 500
      ) {
        const errors = { ...this.state.errors };
        errors.username = " Check the Username and Password.";
        console.log("errors", errors);
        this.setState({ errors: errors });
      }
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.password.trim()) {
      errs.password = "Password is required";
    } else if (this.state.data.password.trim().length < 7)
      errs.password = "Password  must be of  7 Characters";
    if (!this.state.data.name.trim()) {
      errs.email = "Username is required";
    }
    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = e => {
    switch (e.currentTarget.name) {
      case "password":
        if (!e.currentTarget.value.trim()) return "password is required";
        if (e.currentTarget.value.trim().length < 7)
          return "Password  must be of  7 Characters";
        break;
      case "name":
        if (!e.currentTarget.value.trim()) return "Username is required";
        break;
      default:
        break;
    }
    return "";
  };

  handleChange = e => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data: data, errors: errors });
  };

  render() {
    let userInfo = auth.getCurrentUser();
    if (userInfo) {
      let data = userInfo.split("-");
      let role = data[1];
      console.log("userinfo in login");
      if (role === "manager") return <Redirect to="/admin" />;
      if (role === "customer") {
        console.log("customer");
        return <Redirect to="/customer" />;
      }
    }
    const { errors } = this.state;
    return (
      <div className="container">
        <br />
        <br />
        <br />
        <div className="row">
          <h2 className="col-12 text-center">Welcome To GBI Bank</h2>
          <br />
          <br />
          <br />
          {errors.username ? (
            <div className="text-danger col-12 text-center">
              {errors.username}
            </div>
          ) : (
            ""
          )}
        </div>
        <br />
        <form onSubmit={this.handleSubmit}>
          <div className="form-group row">
            <div className="col-4"></div>
            <div className="col-4 text-center">
              <label htmlFor="name">User Name</label>
              <input
                value={this.state.data.name}
                onChange={this.handleChange}
                type="name"
                className="form-control"
                id="name"
                name="name"
                aria-describedby="emailHelp"
                placeholder="Enter user name"
              />
              <small id="emailHelp" className="form-text text-muted">
                We'll never share your email with anyone else.
              </small>
            </div>
            <div className="col-4"></div>
          </div>
          <div className="form-group row">
            <div className="col-4"></div>
            <div className="col-4 text-center">
              <label htmlFor="password">Password</label>
              <input
                value={this.state.data.password}
                onChange={this.handleChange}
                type="password"
                className="form-control"
                id="password"
                name="password"
                placeholder="Password"
              />
              {errors.password ? (
                <div className="text-danger col-12 text-center">
                  {errors.password}
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="col-4"></div>
          <div className="row">
            <div className="col-12 text-center">
              <button
                type="submit"
                className="btn btn-primary btn-xs  text-center"
                disabled={this.isFormValid()}
              >
                LogIn
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default LoginForm;
