import React, { Component } from "react";
import auth from "./../Service/authService";
import http from "./../Service/httpService";
import queryString from "query-string";

class AddCustomer extends Component {
  state = {
    data: {
      name: "",
      password: "",
      conPassword: ""
    },
    errors: {}
  };
  handleSubmit = async e => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    try {
      await auth.addCustomer(this.state.data.name, this.state.data.password);

      window.location = "/admin";
    } catch (ex) {
      if (
        ex.response &&
        ex.response.status >= 400 &&
        ex.response.status <= 500
      ) {
        const errors = { ...this.state.errors };
        errors.username = " Database errors ";
        //console.log("errors", errors);
        this.setState({ errors: errors });
      }
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.password.trim()) {
      errs.password = "Password is required";
    } else if (this.state.data.password.trim().length < 7)
      errs.password =
        "password can not be blank. Minimum length should be 7 Characters";
    if (!this.state.data.name.trim()) {
      errs.name = "Customer Name is required";
    }
    if (!this.state.data.conPassword.trim())
      errs.conPassword = "Confirm password is required";
    if (this.state.data.conPassword.trim() !== this.state.data.password)
      errs.conPassword = "Confirm password same as password";
    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = e => {
    switch (e.currentTarget.name) {
      case "password":
        if (!e.currentTarget.value.trim()) return "password is required";
        if (e.currentTarget.value.trim().length < 7)
          return "password can not be blank. Minimum length should be 7 Characters";
        break;
      case "name":
        if (!e.currentTarget.value.trim()) return "Customer Name  is required";
        break;
      case "conPassword":
        if (!e.currentTarget.value.trim())
          return "Confirm password is required";
        if (e.currentTarget.value.trim() !== this.state.data.password)
          return "Confirm password same as password";
        break;
      default:
        break;
    }
    return "";
  };

  handleChange = e => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data: data, errors: errors });
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">New Customer</h3>
        <br />
        {errors.username ? (
          <div className="text-danger col-12 text-center">
            {errors.username}
          </div>
        ) : (
          ""
        )}
        <div>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                value={this.state.data.name}
                onChange={this.handleChange}
                type="name"
                className="form-control"
                id="name"
                name="name"
                aria-describedby="emailHelp"
                placeholder="Enter customer name"
              />
              {errors.name ? (
                <div className="text-danger ">{errors.name}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                value={this.state.data.password}
                onChange={this.handleChange}
                type="password"
                className="form-control"
                id="password"
                name="password"
              />
              {errors.password ? (
                <div className="text-danger">{errors.password}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="password">Confirm Password</label>
              <input
                value={this.state.data.conPassword}
                onChange={this.handleChange}
                type="password"
                className="form-control"
                id="conPassword"
                name="conPassword"
              />
              {errors.conPassword ? (
                <div className="text-danger ">{errors.conPassword}</div>
              ) : (
                ""
              )}
            </div>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={this.isFormValid()}
            >
              Create
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default AddCustomer;
