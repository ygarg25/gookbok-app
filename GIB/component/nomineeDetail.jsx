import React, { Component } from "react";
import auth from "./../Service/authService";
import http from "./../Service/httpService";

class NomineeDetail extends Component {
  state = {
    data: {
      name: "",
      nomineeName: "",
      gender: "",
      dob: "",
      relationship: "",
      jointSignatory: ""
    },
    errors: {},
    genderData: {
      item: ["Male", "Female"],
      selectGender: ""
    },
    day: [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31
    ],
    month: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ],
    dob: { day: "", month: "", Year: "" },
    update: 1
  };
  handleSubmit = async e => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const userInfo = auth.getCurrentUser();
    if (userInfo) {
      let data = userInfo.split("-");
      let user = data[0];
      let role = data[1];
      this.state.data.name = user;
    }
    const data = { ...this.state.data };
    console.log(data);
    try {
      const { data: response } = await auth.addNomineeDetail(
        data.name,
        data.nomineeName,
        data.gender,
        data.dob,
        data.relationship,
        data.jointSignatory
      );
      console.log("add", response);
      alert("Customer Details are added");

      window.location = "/customer";
    } catch (ex) {
      if (
        ex.response &&
        ex.response.status >= 400 &&
        ex.response.status <= 500
      ) {
        const errors = { ...this.state.errors };
        errors.username = " Database errors ";
        //console.log("errors", errors);
        this.setState({ errors: errors });
      }
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.gender.trim()) {
      errs.gender = "Gender is required";
    }
    if (!this.state.data.nomineeName.trim()) {
      errs.nomineeName = "Nominee Name is required";
    }
    if (!this.state.data.dob.trim()) {
      errs.dob = "DOB is required";
    }
    if (!this.state.data.relationship.trim()) {
      errs.relationship = "Relationship  is required";
    }
    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  handleChange = e => {
    const { currentTarget: input } = e;
    const data = { ...this.state.data };
    const genderData = { ...this.state.genderData };
    const dob = { ...this.state.dob };
    if (input.id === "dob") {
      dob[input.name] = input.value;
      data.dob = dob.day + "-" + dob.month + "-" + dob.Year;
      //console.log("dd", data.staystartdate);
    } else if (input.name === "selectGender") {
      genderData.selectGender = input.value;
      data.gender = input.value;
    } else if (input.type === "checkbox") {
      data[input.name] = input.checked;
    } else {
      data[input.name] = input.value;
    }
    this.setState({
      data: data,
      genderData,
      genderData,
      dob: dob
    });
  };

  async componentWillMount() {
    let apiEndPint = "http://localhost:2450/getNominee";
    const userInfo = auth.getCurrentUser();
    let user;
    let role;
    if (userInfo) {
      let data = userInfo.split("-");
      user = data[0];
      role = data[1];
    }

    apiEndPint = apiEndPint + "/" + user;
    const { data: data } = await http.get(apiEndPint);
    let dob = { ...this.state.dob };

    if (data.dob) {
      let stDate = data.dob.split("-");
      dob.day = stDate[0];
      dob.month = stDate[1];
      dob.Year = stDate[2];
    }

    if (data) {
      this.state.update = 0;
      data.jointSignatory = data.jointsignatory;
      let genderData = { ...this.state.genderData };
      genderData.selectGender = data.gender;
      this.setState({
        data: data,
        dob: dob,
        genderData: genderData
      });
    } else {
      this.setState({
        data: {
          name: "",
          nomineeName: "",
          gender: "",
          dob: "",
          relationship: "",
          jointSignatory: ""
        },
        dob: dob
      });
    }
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    let apiEndPint = "http://localhost:2450/getNominee";
    const userInfo = auth.getCurrentUser();
    let user;
    let role;
    if (userInfo) {
      let data = userInfo.split("-");
      user = data[0];
      role = data[1];
    }

    apiEndPint = apiEndPint + "/" + user;

    if (currProps !== prevProps) {
      const { data: data } = await http.get(apiEndPint);
      let dob = { ...this.state.dob };

      if (data.dob) {
        let stDate = data.dob.split("-");
        dob.day = stDate[0];
        dob.month = stDate[1];
        dob.Year = stDate[2];
      }

      if (data) {
        this.state.update = 0;
        data.jointSignatory = data.jointsignatory;
        let genderData = { ...this.state.genderData };
        genderData.selectGender = data.gender;
        this.setState({
          data: data,
          dob: dob,
          genderData: genderData
        });
      } else {
        this.setState({
          data: {
            name: "",
            nomineeName: "",
            gender: "",
            dob: "",
            relationship: "",
            jointSignatory: ""
          },
          dob: dob
        });
      }
    }
  }

  listOfYear = () => {
    let cuurentYear = new Date().getFullYear();
    let years = [];
    for (let i = 1981; i <= cuurentYear; i++) {
      years.push(i);
    }
    return years;
  };

  listOfDay = () => {
    let totalDays = this.getDaysInMonth(
      this.state.dob.month,
      this.state.dob.Year
    );
    let days = [];
    for (let i = 1; i <= totalDays; i++) {
      days.push(i);
    }
    return days;
  };
  getDaysInMonth = function(month, year) {
    // Here January is 1 based
    //Day 0 is the last day in the previous month
    let monthinnum = 0;
    if (month === "January") monthinnum = 1;
    if (month === "February") monthinnum = 2;
    if (month === "March") monthinnum = 3;
    if (month === "April") monthinnum = 4;
    if (month === "May") monthinnum = 5;
    if (month === "June") monthinnum = 6;
    if (month === "July") monthinnum = 7;
    if (month === "August") monthinnum = 8;
    if (month === "September") monthinnum = 9;
    if (month === "October") monthinnum = 10;
    if (month === "November") monthinnum = 11;
    if (month === "December") monthinnum = 12;
    return new Date(year, monthinnum, 0).getDate();
    // Here January is 0 based
    // return new Date(year, month+1, 0).getDate();
  };

  render() {
    const { errors, genderData } = this.state;

    let year = this.listOfYear();

    let day = this.listOfDay();
    console.log("Ss", this.state.data);
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">Nominee Details</h3>
        <br />
        {errors.username ? (
          <div className="text-danger col-12 text-center">
            {errors.username}
          </div>
        ) : (
          ""
        )}
        <div>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="nomineeName">
                Name
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.nomineeName}
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="nomineeName"
                name="nomineeName"
              />
              {errors.nomineeName ? (
                <div className="alert alert-danger">{errors.nomineeName}</div>
              ) : (
                ""
              )}
            </div>
            <div className="row">
              <div className="col-3">
                <label htmlFor="gender">
                  Gender{" "}
                  <span className="text-danger ">
                    <sup>*</sup>
                  </span>
                </label>
              </div>
              {genderData.item.map(item => (
                <div className="form-check col-3" key={item}>
                  <input
                    value={item}
                    onChange={this.handleChange}
                    id={item}
                    type="radio"
                    name="selectGender"
                    checked={item === genderData.selectGender}
                    className="form-check-input"
                  />
                  <label className="form-check-label" htmlFor={item}>
                    {item}
                  </label>
                </div>
              ))}
            </div>
            <div>
              <hr className="myhr-4" />
            </div>
            <div className="form-group">
              <label htmlFor="dob">
                Date of Birth
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>

              <div className="row">
                <select
                  className="form-control col-4 m-1"
                  value={+this.state.dob.Year}
                  onChange={this.handleChange}
                  id="dob"
                  name="Year"
                  type="Year"
                >
                  <option>Year</option>
                  {year.map((d, index) => (
                    <option key={index}>{d}</option>
                  ))}
                </select>
                <select
                  className="form-control col-4 m-1"
                  value={this.state.dob.month}
                  onChange={this.handleChange}
                  id="dob"
                  name="month"
                  type="month"
                >
                  <option>Month</option>
                  {this.state.month.map((d, index) => (
                    <option key={index}>{d}</option>
                  ))}
                </select>

                <select
                  className="form-control col-3 m-1"
                  value={this.state.dob.day}
                  onChange={this.handleChange}
                  id="dob"
                  name="day"
                  type="day"
                >
                  <option>Day</option>
                  {day.map((d, index) => (
                    <option key={index}>{d}</option>
                  ))}
                </select>
              </div>
            </div>
            <div>
              <hr className="myhr-4" />
            </div>
            <div className="form-group">
              <label htmlFor="relationship">
                Relationship
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.relationship}
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="relationship"
                name="relationship"
              />
              {errors.relationship ? (
                <div className="alert alert-danger">{errors.relationship}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-check">
              <input
                value={this.state.data.jointSignatory}
                onChange={this.handleChange}
                type="checkbox"
                className="form-check-input"
                id="jointSignatory"
                name="jointSignatory"
                checked={this.state.data.jointSignatory === true}
              />
              <label className="form-check-label" htmlFor="jointSignatory">
                Joint Signatory
              </label>
            </div>

            {this.state.update === 1 ? (
              <button
                type="submit"
                className="btn btn-primary"
                disabled={this.isFormValid()}
              >
                Add Detail
              </button>
            ) : (
              ""
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default NomineeDetail;
