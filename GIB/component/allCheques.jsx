import React, { Component } from "react";
import http from "./../Service/httpService";
import queryString from "query-string";

let bankRadio = [];
let amountRadio = [];

class AllCheques extends Component {
  state = {
    chequeData: {},
    bankData: [],
    amountData: ["<10000", ">=10000"]
  };

  async componentWillMount() {
    let { page, bank, amount } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    let apiEndPint = "http://localhost:2450/getAllCheques";
    let apiForBank = "http://localhost:2450/getBanks";

    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "bank", bank);
    params = this.addToParams(params, "amount", amount);

    apiEndPint = apiEndPint + params;
    const { data: chequeData } = await http.get(apiEndPint);
    const { data: bankData } = await http.get(apiForBank);
    this.setState({ chequeData: chequeData, bankData: bankData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;

    let { page, bank, amount } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    let apiEndPint = "http://localhost:2450/getAllCheques";
    let apiForBank = "http://localhost:2450/getBanks";

    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "bank", bank);
    params = this.addToParams(params, "amount", amount);

    apiEndPint = apiEndPint + params;
    if (currProps !== prevProps) {
      const { data: chequeData } = await http.get(apiEndPint);
      const { data: bankData } = await http.get(apiForBank);
      this.setState({ chequeData: chequeData, bankData: bankData });
    }
  }

  makeRadioStructureForBank(bankData, bank) {
    let bankRadio = {
      sectionData: bankData,
      selectedBank: bank
    };
    return bankRadio;
  }

  makeRadioStructureForAmount(amountData, amount) {
    let amountRadio = {
      sectionData: amountData,
      selectedamount: amount
    };
    return amountRadio;
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callUrl = (params, page, bank, amount) => {
    let path = "/allCheques";
    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "bank", bank);
    params = this.addToParams(params, "amount", amount);
    this.props.history.push({ pathname: path, search: params });
  };

  gotoPage = x => {
    let { page, bank, amount } = queryString.parse(this.props.location.search);
    let currentPage = page ? +page : 1;
    currentPage = currentPage + x;
    let params = "";
    this.callUrl(params, currentPage, bank, amount);
  };

  handleChange = e => {
    let paramName = "";
    let { page, bank, amount } = queryString.parse(this.props.location.search);
    const { currentTarget: input } = e;
    if (input.name === "selectedBank") {
      bankRadio[input.name] = input.value;
      bank = input.value;
    }
    if (input.name === "selectedAmount") {
      amountRadio[input.name] = input.value;
      amount = input.value;
    }
    page = 1;
    this.callUrl("", page, bank, amount);
  };

  render() {
    let { page, bank, amount } = queryString.parse(this.props.location.search);
    page = page ? page : 1;
    const { amountData, chequeData, bankData } = this.state;
    console.log("dd", chequeData, amountData, bankData);
    amountRadio = this.makeRadioStructureForAmount(amountData, amount);
    if (bankData) {
      bankRadio = this.makeRadioStructureForBank(bankData, bank);
    }
    let maxPage;
    let item = this.state.chequeData.items;
    if (item) {
      maxPage = Math.ceil(this.state.chequeData.totalNum / 5);
    }
    console.log("bankRadio", bankRadio);
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">All Cheque Transactions</h3>

        <div className="row">
          <div className="col-2">
            <br />
            <form>
              {bankRadio && (
                <ul className="list-group">
                  <li className="list-group-item text-center bg-light">Bank</li>
                  {bankRadio.sectionData.map(item => (
                    <li className="list-group-item text-center" key={item}>
                      <div className="form-check">
                        <input
                          value={item}
                          onChange={this.handleChange}
                          id={item}
                          type="radio"
                          name="selectedBank"
                          checked={item === bankRadio.selectedBank}
                          className="form-check-input"
                        />
                        <label className="form-check-label" htmlFor={item}>
                          {item}
                        </label>
                      </div>
                    </li>
                  ))}
                </ul>
              )}
              <hr className="my-hr4" />
              <ul className="list-group">
                <li className="list-group-item text-center bg-light  ">
                  Amount
                </li>
                {amountRadio.sectionData.map(item => (
                  <li className="list-group-item text-center" key={item}>
                    <div className="form-check">
                      <input
                        value={item}
                        onChange={this.handleChange}
                        id={item}
                        type="radio"
                        name="selectedAmount"
                        checked={item === amountRadio.selectedamount}
                        className="form-check-input"
                      />
                      <label className="form-check-label" htmlFor={item}>
                        {item}
                      </label>
                    </div>
                  </li>
                ))}
              </ul>
            </form>
          </div>
          <div className="col m-2">
            {item && (
              <div>
                <br />
                {(this.state.chequeData.page - 1) * 5 + 1} -{" "}
                {item.length === 5 ? (
                  <span>{this.state.chequeData.page * 5}</span>
                ) : (
                  <span>{this.state.chequeData.totalNum}</span>
                )}{" "}
                of {this.state.chequeData.totalNum}
                <br />
                <div className="row border-top border-bottom">
                  <div className="col-3">Name</div>
                  <div className="col-3">Cheque Number</div>
                  <div className="col-2">Bank Name</div>
                  <div className="col-2">Branch</div>
                  <div className="col-2">Amount</div>
                </div>
                {item.map((i, index) => (
                  <div className="" key={index}>
                    {index % 2 == 0 ? (
                      <div className="row bg-light border-top border-bottom">
                        <div className="col-3">{i.name}</div>
                        <div className="col-3">{i.chequeNumber}</div>
                        <div className="col-2">{i.bankName}</div>
                        <div className="col-2">{i.branch}</div>
                        <div className="col-2">{i.amount}</div>
                      </div>
                    ) : (
                      <div className="row border-top border-bottom">
                        <div className="col-3">{i.name}</div>
                        <div className="col-3">{i.chequeNumber}</div>
                        <div className="col-2">{i.bankName}</div>
                        <div className="col-2">{i.branch}</div>
                        <div className="col-2">{i.amount}</div>
                      </div>
                    )}
                  </div>
                ))}
                <br />
                <div className="row">
                  <div className="col-6 text-left">
                    {+page > 1 ? (
                      <button
                        className="btn btn-secondary"
                        onClick={() => this.gotoPage(-1)}
                      >
                        Previous
                      </button>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="col-6">
                    {page < maxPage ? (
                      <div className="text-right">
                        <button
                          className="btn btn-secondary"
                          onClick={() => this.gotoPage(1)}
                        >
                          Next
                        </button>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default AllCheques;
