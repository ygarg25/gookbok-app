import React, { Component } from "react";
import auth from "./../Service/authService";
import http from "./../Service/httpService";

class AddPayee extends Component {
  state = {
    data: {
      name: "",
      payeeName: "",
      IFSC: "",
      accNumber: "",
      bankName: "GBI"
    },
    bankData: [],
    errors: {},
    bank: { item: ["Same Bank", "Other Bank"], selectBankType: "Same Bank" }
  };
  handleSubmit = async e => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const userInfo = auth.getCurrentUser();
    if (userInfo) {
      let data = userInfo.split("-");
      let user = data[0];
      let role = data[1];
      this.state.data.name = user;
    }
    const data = { ...this.state.data };
    try {
      const { data: response } = await auth.addPayee(
        data.name,
        data.payeeName,
        data.IFSC,
        data.accNumber,
        data.bankName
      );
      alert("Payee added to your list :: " + data.payeeName);

      window.location = "/customer";
    } catch (ex) {
      if (
        ex.response &&
        ex.response.status >= 400 &&
        ex.response.status <= 500
      ) {
        const errors = { ...this.state.errors };
        errors.username = " Database errors ";
        //console.log("errors", errors);
        this.setState({ errors: errors });
      }
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.payeeName.trim()) {
      errs.payeeName = "Payee Name is required";
    }
    if (!this.state.data.accNumber.trim()) {
      errs.accNumber = "Account number is required";
    }
    if (
      this.state.bank.selectBankType === "Other Bank" &&
      !this.state.data.bankName.trim()
    )
      errs.bankName = "Bank Name is required";

    if (
      this.state.bank.selectBankType === "Other Bank" &&
      !this.state.data.IFSC.trim()
    )
      errs.IFSC = "IFSC is required";

    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = e => {
    switch (e.currentTarget.name) {
      case "payeeName":
        if (!e.currentTarget.value.trim()) return "Payee Name is required ";

      case "accNumber":
        if (!e.currentTarget.value.trim()) return "Account number is required";
        break;
      case "bankName":
        if (!e.currentTarget.value.trim()) return "Bank Name is Required";
        break;
      case "IFSC":
        if (
          this.state.bank.selectBankType === "Other Bank" &&
          !e.currentTarget.value.trim()
        )
          return "IFSC is Required";
        break;
      case "bankName":
        if (
          this.state.bank.selectBankType === "Other Bank" &&
          !e.currentTarget.value.trim()
        )
          return "Bank Name is Required";
        break;
      default:
        break;
    }
    return "";
  };

  handleChange = e => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const data = { ...this.state.data };
    const bank = { ...this.state.bank };
    if (input.name === "selectBankType") {
      bank.selectBankType = input.value;
    } else {
      data[input.name] = input.value;
    }
    this.setState({ data: data, errors: errors, bank, bank });
  };

  async componentWillMount() {
    let apiForBank = "http://localhost:2450/getBanks";

    const { data: bankData } = await http.get(apiForBank);
    this.setState({ bankData: bankData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    let apiForBank = "http://localhost:2450/getBanks";

    if (currProps !== prevProps) {
      const { data: bankData } = await http.get(apiForBank);
      this.setState({ bankData: bankData });
    }
  }

  render() {
    const { errors, bankData } = this.state;
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">Add Payee</h3>
        <br />
        {errors.username ? (
          <div className="text-danger col-12 text-center">
            {errors.username}
          </div>
        ) : (
          ""
        )}
        <div>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="payeeName">
                Payee Name
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.payeeName}
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="payeeName"
                name="payeeName"
                placeholder="Enter Payee Name"
              />
              {errors.payeeName ? (
                <div className="alert alert-danger">{errors.payeeName}</div>
              ) : (
                ""
              )}
            </div>

            <div className="form-group">
              <label htmlFor="accNumber">
                Account Number
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.accNumber}
                onChange={this.handleChange}
                type="number"
                className="form-control"
                id="accNumber"
                name="accNumber"
              />
              {errors.accNumber ? (
                <div className="alert alert-danger">{errors.accNumber}</div>
              ) : (
                ""
              )}
            </div>
            <div>
              {this.state.bank.item.map(item => (
                <div className="form-check" key={item}>
                  <input
                    value={item}
                    onChange={this.handleChange}
                    id={item}
                    type="radio"
                    name="selectBankType"
                    checked={item === this.state.bank.selectBankType}
                    className="form-check-input"
                  />
                  <label className="form-check-label" htmlFor={item}>
                    {item}
                  </label>
                </div>
              ))}
            </div>
            <br />
            {this.state.bank.selectBankType === "Other Bank" && (
              <div>
                <div className="form-group">
                  <select
                    value={this.state.data.bankName}
                    onChange={this.handleChange}
                    id="bankName"
                    name="bankName"
                    className="form-control col-12"
                  >
                    <option value="" disabled>
                      Select Bank
                    </option>
                    {bankData.map(n1 => (
                      <option key={n1}>{n1}</option>
                    ))}
                  </select>
                </div>
                <div className="form-group">
                  <label htmlFor="IFSC">
                    IFSC Code
                    <span className="text-danger ">
                      <sup>*</sup>
                    </span>
                  </label>
                  <input
                    value={this.state.data.IFSC}
                    onChange={this.handleChange}
                    type="IFSC"
                    className="form-control"
                    id="IFSC"
                    name="IFSC"
                  />
                  {errors.IFSC ? (
                    <div className="alert alert-danger">{errors.IFSC}</div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            )}
            <br />
            <button
              type="submit"
              className="btn btn-primary"
              disabled={this.isFormValid()}
            >
              Add Payee
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default AddPayee;
