import React, { Component } from "react";
import http from "./../Service/httpService";
import queryString from "query-string";
import auth from "./../Service/authService";

class ViewCheque extends Component {
  state = {
    chequeData: {}
  };

  async componentWillMount() {
    //const task = this.props.match.params.task;
    let { page } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    let apiEndPint = "http://localhost:2450/getChequeByName";
    const userInfo = auth.getCurrentUser();
    let user;
    let role;
    if (userInfo) {
      let data = userInfo.split("-");
      user = data[0];
      role = data[1];
    }

    apiEndPint = apiEndPint + "/" + user;

    params = this.addToParams(params, "page", page);
    apiEndPint = apiEndPint + params;
    const { data: chequeData } = await http.get(apiEndPint);
    this.setState({ chequeData: chequeData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;

    let { page } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    let apiEndPint = "http://localhost:2450/getChequeByName";
    const userInfo = auth.getCurrentUser();
    let user;
    let role;
    if (userInfo) {
      let data = userInfo.split("-");
      user = data[0];
      role = data[1];
    }

    apiEndPint = apiEndPint + "/" + user;
    params = this.addToParams(params, "page", page);
    apiEndPint = apiEndPint + params;

    if (currProps !== prevProps) {
      console.log("check prevProps and currProps", currProps === prevProps);
      const { data: chequeData } = await http.get(apiEndPint);
      this.setState({ chequeData: chequeData });
    }
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callUrl = (params, page) => {
    let path = "/viewCheque";
    params = this.addToParams(params, "page", page);
    this.props.history.push({ pathname: path, search: params });
  };

  gotoPage = x => {
    let { page } = queryString.parse(this.props.location.search);
    let currentPage = page ? +page : 1;
    currentPage = currentPage + x;
    let params = "";
    this.callUrl(params, currentPage);
  };
  render() {
    let { page } = queryString.parse(this.props.location.search);
    page = page ? page : 1;
    let maxPage;
    let { chequeData } = { ...this.state.chequeData };
    //let item = [];
    let item = this.state.chequeData.items;
    console.log(item);
    if (item) {
      maxPage = Math.ceil(this.state.chequeData.totalNum / 5);
    }
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">All Cheque Details</h3>
        <div>
          <br />

          {item && item.length === 0 && (
            <div className="text-danger">No Transactions to show</div>
          )}
          {item && item.length > 0 && (
            <div>
              <br />
              {(this.state.chequeData.page - 1) * 5 + 1} -{" "}
              {item.length === 5 ? (
                <span>{this.state.chequeData.page * 5}</span>
              ) : (
                <span>{this.state.chequeData.totalNum}</span>
              )}{" "}
              of {this.state.chequeData.totalNum}
              <br />
              <div className="row border-top border-bottom">
                <div className="col-4">Cheque Number</div>
                <div className="col-3">Bank Name</div>
                <div className="col-3">Branch</div>
                <div className="col-2">Amount</div>
              </div>
              {item.map((i, index) => (
                <div className="" key={index}>
                  {index % 2 == 0 ? (
                    <div className="row bg-light border-top border-bottom">
                      <div className="col-4">{i.chequeNumber}</div>
                      <div className="col-3">{i.bankName}</div>
                      <div className="col-3">{i.branch}</div>
                      <div className="col-2">{i.amount}</div>
                    </div>
                  ) : (
                    <div className="row border-top border-bottom">
                      <div className="col-4">{i.chequeNumber}</div>
                      <div className="col-3">{i.bankName}</div>
                      <div className="col-3">{i.branch}</div>
                      <div className="col-2">{i.amount}</div>
                    </div>
                  )}
                </div>
              ))}
              <br />
              <div className="row">
                <div className="col-6 text-left">
                  {+page > 1 ? (
                    <button
                      className="btn btn-secondary"
                      onClick={() => this.gotoPage(-1)}
                    >
                      Previous
                    </button>
                  ) : (
                    ""
                  )}
                </div>
                <div className="col-6">
                  {page < maxPage ? (
                    <div className="text-right">
                      <button
                        className="btn btn-secondary"
                        onClick={() => this.gotoPage(1)}
                      >
                        Next
                      </button>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default ViewCheque;
