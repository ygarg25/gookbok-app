import React, { Component } from "react";
import auth from "./../Service/authService";

class LogOut extends Component {
  state = {};

  componentDidMount() {
    auth.logout();
    window.location = "/login";
  }
  render() {
    return null;
  }
}

export default LogOut;
