import React, { Component } from "react";
import auth from "./../Service/authService";
import http from "./../Service/httpService";

class ChequeTransaction extends Component {
  state = {
    data: {
      name: "",
      chequeNumber: "",
      bankName: "",
      branch: "",
      amount: ""
    },
    bankData: [],
    errors: {}
  };
  handleSubmit = async e => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const userInfo = auth.getCurrentUser();
    if (userInfo) {
      let data = userInfo.split("-");
      let user = data[0];
      let role = data[1];
      this.state.data.name = user;
    }
    try {
      await auth.addCheque(
        this.state.data.name,
        this.state.data.chequeNumber,
        this.state.data.bankName,
        this.state.data.branch,
        this.state.data.amount
      );
      alert("Cheque details Added Successfully");

      window.location = "/customer";
    } catch (ex) {
      if (
        ex.response &&
        ex.response.status >= 400 &&
        ex.response.status <= 500
      ) {
        const errors = { ...this.state.errors };
        errors.username = " Database errors ";
        this.setState({ errors: errors });
      }
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.chequeNumber.trim()) {
      errs.chequeNumber = "Enter your 11 digit Cheque Number ";
    } else if (this.state.data.chequeNumber.trim().length < 11)
      errs.chequeNumber = "Enter your 11 digit Cheque Number ";
    if (!this.state.data.branch.trim()) {
      errs.branch = "Enter 4 digit code of branch";
    } else if (this.state.data.branch.trim().length < 4) {
      errs.branch = "Enter 4 digit code of branch";
    }
    if (!this.state.data.amount.trim()) errs.amount = "Amount is required";
    if (!this.state.data.bankName.trim())
      errs.bankName = "Bank Name is required";

    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = e => {
    switch (e.currentTarget.name) {
      case "chequeNumber":
        if (!e.currentTarget.value.trim())
          return "Enter your 11 digit Cheque Number ";
        if (e.currentTarget.value.trim().length < 11)
          return "Enter your 11 digit Cheque Number ";
        break;
      case "branch":
        if (!e.currentTarget.value.trim())
          return " Enter 4 digit code of branch";
        if (e.currentTarget.value.trim().length < 4)
          return "Enter 4 digit code of branch";
        break;
      case "bankName":
        if (!e.currentTarget.value.trim()) return "Bank Name is Required";
        break;
      case "amount":
        if (!e.currentTarget.value.trim()) return "Amount is Required";
        break;
      default:
        break;
    }
    return "";
  };

  handleChange = e => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data: data, errors: errors });
  };

  async componentWillMount() {
    let apiForBank = "http://localhost:2450/getBanks";

    const { data: bankData } = await http.get(apiForBank);
    this.setState({ bankData: bankData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    let apiForBank = "http://localhost:2450/getBanks";

    if (currProps !== prevProps) {
      const { data: bankData } = await http.get(apiForBank);
      this.setState({ bankData: bankData });
    }
  }

  render() {
    const { errors, bankData } = this.state;
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">Deposit Cheque</h3>
        <br />
        {errors.username ? (
          <div className="text-danger col-12 text-center">
            {errors.username}
          </div>
        ) : (
          ""
        )}
        <div>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="chequeNumber">
                Cheque Number
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.chequeNumber}
                onChange={this.handleChange}
                type="Number"
                className="form-control"
                id="chequeNumber"
                name="chequeNumber"
                placeholder="Enter Cheque Number"
              />
              {errors.chequeNumber ? (
                <div className="alert alert-danger">{errors.chequeNumber}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <div>
                <label htmlFor="bankName">
                  Bank Name
                  <span className="text-danger ">
                    <sup>*</sup>
                  </span>
                </label>
              </div>
              <select
                value={this.state.data.bankName}
                onChange={this.handleChange}
                id="bankName"
                name="bankName"
                className="form-control col-12"
              >
                <option value="" disabled>
                  Select Bank
                </option>
                {bankData.map(n1 => (
                  <option key={n1}>{n1}</option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="branch">
                Branch
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.branch}
                onChange={this.handleChange}
                type="branch"
                className="form-control"
                id="branch"
                name="branch"
              />
              {errors.branch ? (
                <div className="alert alert-danger">{errors.branch}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="amount">
                Amount
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.amount}
                onChange={this.handleChange}
                type="number"
                className="form-control"
                id="amount"
                name="amount"
              />
              {errors.amount ? (
                <div className="text-dangealert alert-danger">
                  {errors.amount}
                </div>
              ) : (
                ""
              )}
            </div>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={this.isFormValid()}
            >
              Add Cheque
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default ChequeTransaction;
