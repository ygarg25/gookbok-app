import React from "react";
import auth from "./../Service/authService";
import { Redirect, Route } from "react-router-dom";

// const userInfo = auth.getCurrentUser();
// let user;
// let role;
// if (userInfo) {
//   let data = userInfo.split("-");
//   user = data[0];
//   role = data[1];
// }
const ProtectedRoute = ({ path, component: Component, render, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        console.log(props);
        if (!auth.getCurrentUser())
          return (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            />
          );
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRoute;
