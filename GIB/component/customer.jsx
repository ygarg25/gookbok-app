import React, { Component } from "react";

class Customer extends Component {
  render() {
    console.log("customer");
    return (
      <div className="container">
        <br />
        <br />
        <div className="row">
          <h2 className="col-12 text-center text-danger">
            Welcome to GBI Bank Customer Portal
          </h2>
        </div>
        <br />
        <br />
        <div className="row">
          <div className="col-12 text-center">
            <img
              src={
                "https://thumbs.dreamstime.com/z/bank-building-icon-24113387.jpg"
              }
              style={{ width: "300px" }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Customer;
