import React, { Component } from "react";
import auth from "./../Service/authService";
import http from "./../Service/httpService";

class NetBAnkingTransaction extends Component {
  state = {
    data: {
      name: "",
      payeeName: "",
      comment: "",
      amount: "",
      bankName: ""
    },
    bankData: [],
    errors: {},
    payeeData: []
  };
  handleSubmit = async e => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    const userInfo = auth.getCurrentUser();
    if (userInfo) {
      let data = userInfo.split("-");
      let user = data[0];
      let role = data[1];
      this.state.data.name = user;
    }
    let payeedetail = this.state.payeeData.filter(
      n1 => n1.payeeName === this.state.data.payeeName
    );
    this.state.data.bankName = payeedetail[0].bankName;
    console.log(this.state.data, payeedetail);
    try {
      await auth.addNetBanking(
        this.state.data.name,
        this.state.data.payeeName,
        this.state.data.comment,
        this.state.data.amount,
        this.state.data.bankName
      );
      alert("details Added Successfully");

      window.location = "/customer";
    } catch (ex) {
      if (
        ex.response &&
        ex.response.status >= 400 &&
        ex.response.status <= 500
      ) {
        const errors = { ...this.state.errors };
        errors.username = " Database errors ";
        this.setState({ errors: errors });
      }
    }
  };

  validate = () => {
    let errs = {};
    if (!this.state.data.payeeName.trim()) {
      errs.chequeNumber = "Payee Name is Required";
    }
    if (!this.state.data.amount.trim()) errs.amount = "Amount is required";

    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = e => {
    switch (e.currentTarget.name) {
      case "payeeName":
        if (!e.currentTarget.value.trim()) return "Payee Name is Required";
        break;
      case "amount":
        if (!e.currentTarget.value.trim()) return "Amount is Required";
        break;
      default:
        break;
    }
    return "";
  };

  handleChange = e => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data: data, errors: errors });
  };

  async componentWillMount() {
    let apiForBank = "http://localhost:2450/getBanks";
    let apiForPAyee = "http://localhost:2450/getPayees";
    const userInfo = auth.getCurrentUser();
    let user;
    let role;
    if (userInfo) {
      let data = userInfo.split("-");
      user = data[0];
      role = data[1];
    }

    apiForPAyee = apiForPAyee + "/" + user;

    const { data: bankData } = await http.get(apiForBank);
    const { data: payeeData } = await http.get(apiForPAyee);
    this.setState({ bankData: bankData, payeeData: payeeData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;
    let apiForBank = "http://localhost:2450/getBanks";
    let apiForPAyee = "http://localhost:2450/getPayees";
    const userInfo = auth.getCurrentUser();
    let user;
    let role;
    if (userInfo) {
      let data = userInfo.split("-");
      user = data[0];
      role = data[1];
    }

    apiForPAyee = apiForPAyee + "/" + user;

    if (currProps !== prevProps) {
      const { data: bankData } = await http.get(apiForBank);
      const { data: payeeData } = await http.get(apiForPAyee);
      this.setState({ bankData: bankData, payeeData: payeeData });
    }
  }

  render() {
    const { errors, bankData, payeeData } = this.state;
    return (
      <div className="container">
        <br />
        <h3 className="text-dark">Net Banking Detail</h3>
        <br />
        {errors.username ? (
          <div className="text-danger col-12 text-center">
            {errors.username}
          </div>
        ) : (
          ""
        )}
        <div>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <div>
                <label htmlFor="bankName">
                  Payee Name
                  <span className="text-danger ">
                    <sup>*</sup>
                  </span>
                </label>
              </div>
              <select
                value={this.state.data.payeeName}
                onChange={this.handleChange}
                id="payeeName"
                name="payeeName"
                className="form-control col-12"
              >
                <option value="" disabled>
                  Select Payee
                </option>
                {payeeData.map((n1, index) => (
                  <option key={index}>{n1.payeeName}</option>
                ))}
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="amount">
                Amount
                <span className="text-danger ">
                  <sup>*</sup>
                </span>
              </label>
              <input
                value={this.state.data.amount}
                onChange={this.handleChange}
                type="number"
                className="form-control"
                id="amount"
                name="amount"
              />
              {errors.amount ? (
                <div className="alert alert-danger">{errors.amount}</div>
              ) : (
                ""
              )}
            </div>
            <div className="form-group">
              <label htmlFor="comment">Comment</label>
              <input
                value={this.state.data.comment}
                onChange={this.handleChange}
                type="text"
                className="form-control"
                id="comment"
                name="comment"
              />
            </div>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={this.isFormValid()}
            >
              Add Cheque
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default NetBAnkingTransaction;
