import React from "react";
import { Link } from "react-router-dom";

const NavBar = ({ user, role }) => {
  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-warning">
      <Link className="navbar-brand" to="/">
        Home
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          {!user && (
            <li className="nav-item">
              <Link className="nav-link" to="/login">
                Login
              </Link>
            </li>
          )}
          {user && (
            <React.Fragment>
              {role === "manager" ? (
                <div className="row">
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle"
                      to="/admin"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Customers
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link className="dropdown-item" to="/addCustomer">
                        Add Customers
                      </Link>
                      <Link className="dropdown-item" to="/allCustomers?page=1">
                        View All Customers
                      </Link>
                    </div>
                  </li>
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle"
                      to="/admin"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Transactions
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link className="dropdown-item" to="/allCheques?page=1">
                        Cheques
                      </Link>
                      <Link className="dropdown-item" to="/allNet?page=1">
                        Net Banking
                      </Link>
                    </div>
                  </li>
                </div>
              ) : (
                <div className="row">
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle"
                      to="/customer"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      View
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link className="dropdown-item" to="/viewCheque?page=1">
                        Cheque
                      </Link>
                      <Link className="dropdown-item" to="/viewNet?page=1">
                        Net Banking
                      </Link>
                    </div>
                  </li>
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle"
                      to="/customer"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Details
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link className="dropdown-item" to="/customerDetails">
                        Customer
                      </Link>
                      <Link className="dropdown-item" to="/nomineeDetails">
                        Nominee
                      </Link>
                    </div>
                  </li>
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle"
                      to="/customer"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Transaction
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link className="dropdown-item" to="/addPayee">
                        Add Payee
                      </Link>
                      <Link className="dropdown-item" to="/cheque">
                        Cheque
                      </Link>
                      <Link className="dropdown-item" to="/netBanking">
                        NetBanking
                      </Link>
                    </div>
                  </li>
                </div>
              )}
            </React.Fragment>
          )}
        </ul>
        <div className="nav-item">
          {user && (
            <div className="row">
              <Link className="nav-link text-dark" to="">
                Welcome {user}
              </Link>
              <Link className="nav-link text-dark" to="/logout">
                LogOut
              </Link>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
